package com.NatSolutions.TheLemonTree.base;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by amr on 17/05/17.
 */

public class ConnectionManager {

    private static ApiManager apiManager;
    private static String BASE_URL = "http://reservationapp.natlab.net/api/";
//    private static String BASE_URL = "http://resdemo.natlab.net/api/";


    public static ApiManager getApiManager() {
        if (apiManager == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
            apiManager = retrofit.create(ApiManager.class);
        }
        return apiManager;

    }


}
