package com.NatSolutions.TheLemonTree.base;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.NatSolutions.TheLemonTree.utils.MessageEvent;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;
import com.NatSolutions.TheLemonTree.view.view.MyReservationsActivity;
import com.NatSolutions.TheLemonTree.view.view.reservation.BarCodeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

import static com.NatSolutions.TheLemonTree.model.Constants.NOTIFICATION_RECEIVED;
import static com.NatSolutions.TheLemonTree.model.Constants.RESERVATION_CANCELLED;
import static com.NatSolutions.TheLemonTree.model.Constants.RESERVATION_CONFIRMED;
import static com.NatSolutions.TheLemonTree.model.Constants.RESERVATION_DISCONFIRMED;

/**
 * Created by Hagar on 6/7/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
//        Log.d(TAG, "From: " + remoteMessage.getFrom());


        /*
         { to = "ecDwoPW0.....", // Client Token
        notification = {{
        body = Your reservation has been confirmed.,
         title = The Lemon Tree & co.,
        sound = Enabled,
        ClientId = 42063,
        ReservationId = 1949,
         NotificationType = Confirmed, // based on this type el redirect 3la el screens hyt7add
         NotificationTypeId = 1
         }} }
 */


        // Map<String,String> data1 = remoteMessage.getData();

        //NotificationModel notification = new Gson().fromJson(remoteMessage.getData(), NotificationModel.class);

        if (remoteMessage.getData() != null) {
            sendNotification(remoteMessage.getData());
            DataUtil.saveData(LemonTreeApp.context,NOTIFICATION_RECEIVED,true);
            EventBus.getDefault().post(new MessageEvent(NOTIFICATION_RECEIVED));
        }
//         if(remoteMessage.getNotification() != null){
//            if(remoteMessage.getNotification().getBody().contains("cancel")){
//                DataUtil.saveData(this,EXTRA_CANCEL_NOTIFICATION,EXTRA_CANCEL_NOTIFICATION);
//            }
//        }

    }

    private void sendNotification(Map<String, String> data) {

        switch (data.get("NotificationType")) {
            case RESERVATION_CONFIRMED:
                int notificationId = 005;
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setAutoCancel(true)
                                .setSmallIcon(R.drawable.app_icon)
                                .setContentTitle(data.get("title"))
                                .setContentText(data.get("body"));

                Intent resultIntent = new Intent(this, BarCodeActivity.class);
                resultIntent.putExtra("name", data.get("VenueName"));
                resultIntent.putExtra("id", data.get("ReservationId"));
                resultIntent.putExtra("type", data.get("NotificationType"));
                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                mBuilder.setContentIntent(resultPendingIntent);
                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder.setSound(alarmSound);
                mBuilder.setAutoCancel(true);

                NotificationManager mNotifyMgr =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotifyMgr.notify(notificationId, mBuilder.build());

                break;

            case RESERVATION_DISCONFIRMED:
                int notificationId2 = 006;
                NotificationCompat.Builder mBuilder2 =
                        new NotificationCompat.Builder(this)
                                .setAutoCancel(true)
                                .setSmallIcon(R.drawable.app_icon)
                                .setContentTitle(data.get("title"))
                                .setContentText(data.get("body"));

                Intent resultIntent2 = new Intent(this, MyReservationsActivity.class);
                PendingIntent resultPendingIntent2 =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent2,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                mBuilder2.setContentIntent(resultPendingIntent2);
                Uri alarmSound2 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder2.setSound(alarmSound2);
                mBuilder2.setAutoCancel(true);

                NotificationManager mNotifyMgr2 =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotifyMgr2.notify(notificationId2, mBuilder2.build());
                break;


            case RESERVATION_CANCELLED:
                int notificationId3 = 006;
                NotificationCompat.Builder mBuilder3 =
                        new NotificationCompat.Builder(this)
                                .setAutoCancel(true)
                                .setSmallIcon(R.drawable.app_icon)
                                .setContentTitle(data.get("title"))
                                .setContentText(data.get("body"));

                Intent resultIntent3 = new Intent(this, MyReservationsActivity.class);
                PendingIntent resultPendingIntent3 =
                        PendingIntent.getActivity(
                                this,
                                0,
                                resultIntent3,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );

                mBuilder3.setContentIntent(resultPendingIntent3);
                Uri alarmSound3 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder3.setSound(alarmSound3);
                mBuilder3.setAutoCancel(true);

                NotificationManager mNotifyMgr3 =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                mNotifyMgr3.notify(notificationId3, mBuilder3.build());
                break;

            // TODO: 3/24/18 Uncomment code for deposit notification
//            case RESERVATION_DEPOSIT:
//                int notificationId4 = 005;
//                NotificationCompat.Builder mBuilder4 =
//                        new NotificationCompat.Builder(this)
//                                .setAutoCancel(true)
//                                .setSmallIcon(R.drawable.app_icon)
//                                .setContentTitle(data.get("title"))
//                                .setContentText(data.get("body"));
//
////                String x = data.get("PaymentURL");
////                Map<String,String> y = data;
//
//                Intent resultIntent4 = new Intent(this, BarCodeActivity.class);
//                resultIntent4.putExtra("name", data.get("VenueName"));
//                resultIntent4.putExtra("id", data.get("ReservationId"));
//                resultIntent4.putExtra("type", data.get("NotificationType"));
//                resultIntent4.putExtra("payment_url", data.get("PaymentURL"));
//                PendingIntent resultPendingIntent4 =
//                        PendingIntent.getActivity(
//                                this,
//                                0,
//                                resultIntent4,
//                                PendingIntent.FLAG_UPDATE_CURRENT
//                        );
//
//                mBuilder4.setContentIntent(resultPendingIntent4);
//                Uri alarmSound4 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                mBuilder4.setSound(alarmSound4);
//                mBuilder4.setAutoCancel(true);
//
//                NotificationManager mNotifyMgr4 =
//                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//                mNotifyMgr4.notify(notificationId4, mBuilder4.build());
//
//                break;

        }
    }

}
