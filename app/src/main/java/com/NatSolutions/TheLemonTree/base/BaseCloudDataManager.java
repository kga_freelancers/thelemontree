package com.NatSolutions.TheLemonTree.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Rowan Tarek on 29/4/2017
 */

public class BaseCloudDataManager {
    //=================================================================//
    private static BaseCloudDataManager instance;
    private final RequestQueue requestQueue;
    private static String BASE_URL = "http://reservationapp.natlab.net/api/";
//    private static String BASE_URL = "http://resdemo.natlab.net/api/";

    //=================================================================//
    private BaseCloudDataManager(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    //=================================================================//
    public static BaseCloudDataManager getInstance(Context context) {
        if (instance == null) {
            instance = new BaseCloudDataManager(context);
        }
        return instance;
    }

    //=================================================================//
    public void requestJson(String serviceUrl, @NonNull final OnRequestCompletedListener listener)
            throws JSONException {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, BASE_URL + serviceUrl, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess("", response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onFail(error.getMessage().toString());

                    }//end onErrorResponse
                });
        requestQueue.add(jsObjRequest);
    }//requestJson

    //------------------------------------------------------------------//
    public <T> void requestJson(String serviceUrl, @NonNull final OnRequestCompletedListener listener,
                                T requestBody) throws JSONException {
        JSONObject jsonRequestBody = null;
        if (requestBody != null) {
            Gson gson = new Gson();
            jsonRequestBody = new JSONObject(gson.toJson(requestBody));
        }//end if --> no request body

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, BASE_URL + serviceUrl, jsonRequestBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String message = "", state = "true", type = "";
                        JSONArray array = new JSONArray();
                        try {
                            if (response.has("Message")) {
                                message = response.getString("Message").toString();
                            }
                            if (response.has("Status")) {
                                state = response.getString("Status").toString();
                            }
                            if (response.has("Obj")) {
                                JSONObject OBJ = response.getJSONObject("Obj");
                                if (OBJ.has("Venues")) {
                                    array = OBJ.getJSONArray("Venues");
                                    type = "Venues";

                                } else if (OBJ.has("Events")) {
                                    array = OBJ.getJSONArray("Events");
                                    type = "events";
                                } else if (OBJ.has("Types")) {
                                    array = OBJ.getJSONArray("Types");
                                    type = "reservationData";
                                } else if (OBJ.has("concepts")) {
                                    array = OBJ.getJSONArray("concepts");
                                    type = "concepts";
                                } else if (OBJ.has("Restaurants")) {
                                    array = OBJ.getJSONArray("Restaurants");
                                    type = "Restaurants";
                                } else if (OBJ.has("concepts")) {
                                    array = OBJ.getJSONArray("concepts");
                                    type = "concepts";
                                } else if (OBJ.has("ClientReservations")) {
                                    array = OBJ.getJSONArray("ClientReservations");
                                    type = "ClientReservations";
                                }
                            }
                        } catch (Exception e) {

                        }
                        if (state.equalsIgnoreCase("true")) {
                            listener.onSuccessArray(array, message, type);
                        } else {
                            listener.onSuccessArray(array, message, type);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getMessage() != null) {
                            listener.onFail(error.getMessage().toString());
                        }


                    }//end onErrorResponse
                });
        requestQueue.add(jsObjRequest);
    }//requestJson

    //=================================================================//
    public <T> void requestJsonPost(String serviceUrl, @NonNull final OnRequestCompletedListener listener,
                                    T requestBody) throws JSONException {
        JSONObject jsonRequestBody = null;
        if (requestBody != null) {
            Gson gson = new Gson();
            try {
                jsonRequestBody = new JSONObject(requestBody.toString());
            } catch (Exception e) {
                Log.e("", "" + e.toString());
            }
        }//end if --> no request body

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL + serviceUrl, jsonRequestBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onSuccess("", response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getMessage() != null) {

                            listener.onFail("");


                        } else
                            listener.onFail("Errorr Occured");

                    }//end onErrorResponse
                });
        requestQueue.add(jsObjRequest);
    }//requestJson

    //=================================================================//
    public <T> void requestJsonPut(String serviceUrl, @NonNull final OnRequestCompletedListener listener,
                                   T requestBody) throws JSONException {
        JSONObject jsonRequestBody = null;
        if (requestBody != null) {
            Gson gson = new Gson();
            try {
                jsonRequestBody = new JSONObject(requestBody.toString());
            } catch (Exception e) {
                Log.e("", "" + e.toString());
            }
        }//end if --> no request body

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.PUT, BASE_URL + serviceUrl, jsonRequestBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String msg = "";
                        try {
                            if (response.has("Message"))
                                msg = response.getString("Message").toString();
                        } catch (Exception e) {

                        }
                        listener.onSuccess(msg, response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getMessage() != null) {

                            listener.onFail("");


                        } else
                            listener.onFail("Errorr Occured");

                    }//end onErrorResponse
                });
        requestQueue.add(jsObjRequest);
    }//requestJson

    public <T> void requestJsonObject(String serviceUrl, @NonNull final OnRequestCompletedListener listener,
                                      T requestBody) throws JSONException {
        JSONObject jsonRequestBody = null;
        if (requestBody != null) {
            Gson gson = new Gson();
            jsonRequestBody = new JSONObject(gson.toJson(requestBody));
        }//end if --> no request body

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, BASE_URL + serviceUrl, jsonRequestBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String message = "", state = "true", type = "";
                        try {
                            if (response.has("Message")) {
                                message = response.getString("Message").toString();
                            }

                            listener.onSuccess(message, response);
                        } catch (Exception e) {

                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getMessage() != null) {
                            listener.onFail(error.getMessage().toString());
                        }


                    }//end onErrorResponse
                });
        requestQueue.add(jsObjRequest);
    }//requestJson


    //=================================================================//
    public <T> void requestJsonPostAerray(String serviceUrl, @NonNull final OnRequestCompletedListener listener,
                                          T requestBody) throws JSONException {
        JSONArray jsonRequestBody = null;
        serviceUrl = serviceUrl.replaceAll(" ", "%20");
        if (requestBody != null) {
            Gson gson = new Gson();
            try {
                jsonRequestBody = new JSONArray(requestBody.toString());
            } catch (Exception e) {
                Log.e("", "" + e.toString());
            }
        }//end if --> no request body

        JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.POST, BASE_URL + serviceUrl, jsonRequestBody, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        listener.onSuccess("", response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            String value = ((((ParseError) error)).getCause().getMessage().toString()).substring(5, ((((((ParseError) error)).getCause()).getMessage())).toString().length() - 61);
                            try {
                                JSONObject response = new JSONObject(value);

                                String message = "", state = "true", type = "";
                                String bookingId = null;
                                try {
                                    if (response.has("Message")) {
                                        message = response.getString("Message").toString();
                                    }
                                    if (response.has("Status")) {
                                        state = response.getString("Status").toString();
                                    }
                                    if (response.has("Obj")) {
                                        JSONObject OBJ = response.getJSONObject("Obj");
                                        if (OBJ.has("ReservationBookingId")) {
                                            bookingId = OBJ.getString("ReservationBookingId");
                                        }
                                    }
                                } catch (Exception e) {

                                }
                                if (state.equalsIgnoreCase("true")) {
                                    listener.onSuccess(message, bookingId);
                                } else {
                                    listener.onSuccess(message, null);
                                }
                            } catch (Exception e) {

                            }

                        } catch (Exception e) {

                            if (error.getMessage() != null) {

                                listener.onFail("" + error.getMessage());


                            } else
                                listener.onFail("Errorr Occured");

                        }
                    }
                    // /end onErrorResponse
                });
        requestQueue.add(jsObjRequest);
    }//requestJson
}