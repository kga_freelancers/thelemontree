package com.NatSolutions.TheLemonTree.base;

import org.json.JSONArray;

/**
 * Created by Rowan Tarek on 29/4/2017
 */

public interface OnRequestCompletedListener {
    void onSuccess(String message,Object response);
    void onSuccess(String message,String response);

    void onSuccessArray(JSONArray response, String message, String type);

    void onFail(String ex);
}
