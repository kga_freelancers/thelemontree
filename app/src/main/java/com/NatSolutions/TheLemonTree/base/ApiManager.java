package com.NatSolutions.TheLemonTree.base;


import com.NatSolutions.TheLemonTree.model.FavoriteTrackResponse;
import com.NatSolutions.TheLemonTree.model.MyResponse;
import com.NatSolutions.TheLemonTree.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.model.TracksResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by amr on 13/06/17.
 */

public interface ApiManager {
    @GET("Client/GetClientNotificationsByClientId")
    Call<NotificationsResponse> getUserNotifications(@Query("ClientId") int clientId);

    @GET("Client/GetClientFavouriteMusicByClientId")
    Call<FavoriteTrackResponse> getUserFavouriteMusic(@Query("ClientId") int clientId);

    @POST("Client/AddFavouriteTrack")
    Call<MyResponse> addFavoriteTrack(@Query("TrackId") int trackID, @Query("ClientId") int clientId);

    @GET("Track/GetAllTracks")
    Call<TracksResponse> getAllTracks();

    @DELETE("Client/RemoveFavouriteTrack")
    Call<MyResponse> removeFavoriteTrack(@Query("TrackId") int trackID, @Query("ClientId") int clientId);

    @GET("Booking/CheckPaymentStatus")
    Call<MyResponse> checkPaymentStatus(@Query("reservationId") int reservationID,
                                        @Query("payemtAmount") int clientId);


}
