package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hagar on 5/11/2017.
 */

public class EventItem implements Serializable {
    @SerializedName("EventId")
    public String EventId;
    @SerializedName("EventName")
    public String EventName;
    @SerializedName("EventDate")
    public String EventDate;
    @SerializedName("EventTime")
    public String EventTime;
    @SerializedName("EventDescription")
    public String EventDescription;
    @SerializedName("VenueId")
    public String VenueId;
    @SerializedName("InVenue")
    public String InVenue;
    @SerializedName("EventLocation")
    public String EventLocation;
    @SerializedName("Latitude")
    public String Latitude;
    @SerializedName("Longitude")
    public String Longitude;
    @SerializedName("EventMaxCapacity")
    public String EventMaxCapacity;
    @SerializedName("EventPhotoPath")
    public String EventPhotoPath;
    @SerializedName("isActive")
    public String isActive;
    @SerializedName("CreatedBy")
    public String CreatedBy;
    @SerializedName("CreationDate")
    public String CreationDate;
    @SerializedName("Venue")
    public String Venue;
    @SerializedName("EventBookSettings")
    public EventBookSettingsItem EventBookSettings;
    @SerializedName("EventReservationTypes")
    public EventReservationTypesItem EventReservationTypes;
    @SerializedName("EventsBookings")
    public EventsBookingsItem EventsBookings;
    public int photoId;
    public String telephone;
    private String latlang;
    public  String title;


    public  EventItem(String name, String desc, Integer photo, String latlang, String address,String Latitude,String Longitude) {
        this.EventName = name;
        this.EventDescription = desc;
        this.photoId = photo;
        this.title = latlang;
        this.EventLocation = address;
        this.Latitude=Latitude;
        this.Longitude=Longitude;
    }


}
