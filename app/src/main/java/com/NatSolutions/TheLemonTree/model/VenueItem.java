package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/11/2017.
 */

public class VenueItem {
    @SerializedName("VenueName")
    private String VenueName;

    public String getVenueName() {
        return VenueName;
    }

    public void setVenueName(String venueName) {
        VenueName = venueName;
    }

    public String getVenueId() {
        return VenueId;
    }

    public void setVenueId(String venueId) {
        VenueId = venueId;
    }

    @SerializedName("VenueId")
    private String VenueId;
}
