package com.NatSolutions.TheLemonTree.model;

/**
 * Created by karim on 22/07/17.
 */
public class HomeItemsModel {
    String itemName , itemDescription;
    int imageSource;

    public HomeItemsModel(String itemName , String itemDescription, int imageSource) {
        this.itemName = itemName;
        this.imageSource = imageSource;
        this.itemDescription = itemDescription;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemImage() {
        return imageSource;
    }

    public void setItemImage(int imageSource) {
        this.imageSource = imageSource;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }
}
