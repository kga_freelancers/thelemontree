package com.NatSolutions.TheLemonTree.model;


public class DataModel {


    public int getImg() {
        return img;
    }

    private final int img;
    String name;
    String version;

    public DataModel(String name, String description, int imag) {
        this.name = name;
        this.version = description;
        this.img = imag;
    }


    public String getName() {
        return name;
    }


    public String getVersion() {
        return version;
    }


}