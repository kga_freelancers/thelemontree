package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ResturantModel {
    @SerializedName("VenueId")
    public String VenueId;
    @SerializedName("VenueName")
    public String VenueName;
    @SerializedName("VenuePhone")
    public String VenuePhone;
    @SerializedName("VenueMobile")
    public String VenueMobile;
    @SerializedName("VenueAddress")
    public String VenueAddress;
    @SerializedName("Latitude")
    public String Latitude;
    @SerializedName("Longitude")
    public String Longitude;
    @SerializedName("MaxCapacity")
    public String MaxCapacity;
    @SerializedName("NumberOfTables")
    public String NumberOfTables;
    @SerializedName("IsActiveForBooking")
    public boolean IsActiveForBooking;
    @SerializedName("IsOurRestaurants")
    public boolean IsOurRestaurants;
    @SerializedName("RestaurantDescription")
    public String RestaurantDescription;
    @SerializedName("RestaurantPhotoPath")
    public String RestaurantPhotoPath;
    @SerializedName("CreatedBy")
    public String CreatedBy;
    @SerializedName("CreationDate")
    public String CreationDate;
    @SerializedName("Events")
    public ArrayList<EventItem> Events;
    @SerializedName("ReservationsBookings")
    public ArrayList<ReservationResponse> ReservationsBookings;
    @SerializedName("VenueBookSettings")
    public ArrayList<VenueItem> VenueBookSettings;
    @SerializedName("VenueReservationTypes")
    public ArrayList<ReservationTypeItem> VenueReservationTypes;
    @SerializedName("ImageFile")
    public String ImageFile;


}
