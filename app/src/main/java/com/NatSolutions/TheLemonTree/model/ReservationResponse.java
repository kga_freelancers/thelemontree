package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 4/30/2017.
 */

public class ReservationResponse {
    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public UserReservationSuccess getObj() {
        return Obj;
    }

    public void setObj(UserReservationSuccess obj) {
        Obj = obj;
    }

    @SerializedName("Message")
    private String Message;

    @SerializedName("Status")
    private String Status;

    private UserReservationSuccess Obj;
}
