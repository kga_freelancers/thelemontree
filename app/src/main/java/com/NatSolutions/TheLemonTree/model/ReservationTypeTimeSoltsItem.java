package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/11/2017.
 */

public class ReservationTypeTimeSoltsItem {
    @SerializedName("TimeSlotValue")
    private  String TimeSlotValue;

    public String getTimeSlotValue() {
        return TimeSlotValue;
    }

    public void setTimeSlotValue(String timeSlotValue) {
        TimeSlotValue = timeSlotValue;
    }

    public String getTimeSlotId() {
        return TimeSlotId;
    }

    public void setTimeSlotId(String timeSlotId) {
        TimeSlotId = timeSlotId;
    }

    @SerializedName("TimeSlotId")
    private  String TimeSlotId;
}
