package com.NatSolutions.TheLemonTree.model;

import android.content.Context;

import com.NatSolutions.TheLemonTree.base.BaseCloudDataManager;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;

import org.json.JSONException;

/**
 * Created by Rowan Tarek on 29/4/2017
 */

public class UserAccountDataManager {
    //=================================================================//
    private final String REGISTER_WITH_FB_URL = "Client/RegisterClientByFacebook";
    private final String LOGIN_WITH_FB_URL = "Client/LoginCLientByFacebookId";
    private final String UpdateUserInfo_URL = "Client/UpdateClient";

    //=================================================================//
    public void registerWithFacebook(Context context, Object fbUserObj,
                                     OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJsonPost(REGISTER_WITH_FB_URL, listener, fbUserObj);
    }//end registerWithFacebook
    // =================================================================//



    //=================================================================//
    public void loginWithFacebook(Context context, String fbUserId,String token,
                                     OnRequestCompletedListener listener) throws JSONException {


        BaseCloudDataManager.getInstance(context).requestJsonObject(LOGIN_WITH_FB_URL+"?ClientFacebookId="+fbUserId+"&ClientFCMToken="+token, listener,null);
    }//end registerWithFacebook
    // =================================================================//
    //=================================================================//
    public void UpdateUserProfile(Context context,Object obh,
                                  OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJsonPut(UpdateUserInfo_URL, listener, obh);
    }
}//end UserAccountDataManager
