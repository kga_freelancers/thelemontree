package com.NatSolutions.TheLemonTree.model;

import android.content.Context;

import com.NatSolutions.TheLemonTree.base.BaseCloudDataManager;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;

import org.json.JSONException;


public class AppDataManager {
    //=================================================================//
    private final String GetAllConcepts_URL = "Concept/GetConceptInfo";
    private final String GetAllRestaurants_URL = "Venue/GetAllRestaurants";
    private final String GetAllEvents_URL = "Event/GetAllEvents";
    private final String GetAllBookingsForUser_URL = "Client/GetClientBookingByClientId";
    private final String GetReservationDetailsByID_URL = "Booking/GetReservationInfoById";
    private final String CancelReservation_URL = "Booking/CancelReservation";
    private final String GetUserInfo_URL = "Client/GetClientInfoById";

    //=================================================================//
    public void GetAllConcepts(Context context,
                               OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJson(GetAllConcepts_URL, listener, null);
    }

    // =================================================================//
//=================================================================//
    public void GetAllEvnets(Context context,
                             OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJson(GetAllEvents_URL, listener, null);
    }

    // =================================================================//
    //=================================================================//
    public void GetAllResturants(Context context,
                                 OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJson(GetAllRestaurants_URL, listener, null);
    }

    //=================================================================//
    public void GetUserBookings(Context context, String clientId,
                                OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJson(GetAllBookingsForUser_URL + "?ClientId=" + clientId, listener, null);
    }

    //=======================================================================//
    //=================================================================//
    public void GetReservationDetails(Context context, String reservationId,
                                      OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJsonObject(GetReservationDetailsByID_URL + "?ReservationId=" + reservationId, listener, null);
    }

    //=================================================================//
    public void CnacelReservation(Context context, String reservationId,
                                  OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJson(CancelReservation_URL + "?reservationId=" + reservationId + "&CancellationReson=" + "", listener, null);
    }
}//end UserAccountDataManager
