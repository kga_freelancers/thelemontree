package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ProfileItem {
    @SerializedName("ClientId")
    public String ClientId;
    @SerializedName("ClientPhoneNumber")
    public String ClientPhoneNumber;
    @SerializedName("ClientEmail")
    public String ClientEmail;
}
