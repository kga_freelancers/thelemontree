package com.NatSolutions.TheLemonTree.model;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/7/2017.
 */

public class Constants {

    public static final String LEMON_TREE_BOLD = "lm_bold";
    public static final String LEMON_TREE_REGULAR = "lm_regular";
    public static final String RESERVATION_CONFIRMED = "Confirmed";
    public static final String RESERVATION_DISCONFIRMED = "Disconfirmed";
    public static final String RESERVATION_CANCELLED = "Cancelled";
    public static final String RESERVATION_DEPOSIT = "Deposit";
    public static final String RESERVATION_DEPOSITPAID = "DepositPaid";
    public static final String RESERVATION_GENERAL = "General";
    public static final String NOTIFICATION_RECEIVED = "notification_received";
    public static final String EXTRA_TRACK = "extra_track";
    public static final String EXTRA_TRACK_LIST = "extra_track_list";
    public static final String EXTRA_NOTIFICATION = "extra_notification";
    public static final String EXTRA_CANCEL_NOTIFICATION = "extra_cancel_notification";
    public static final int ITEM_NOTIFICATION_SIDE_MENU = 0;
    public static final int ITEM_RESERVATIONS_SIDE_MENU = 1;
    public static final int ITEM_MUSIC_SIDE_MENU = 2;


    private static Constants con;


    public EventItem getReservedEvent() {
        return ReservedEvent;
    }

    public void setReservedEvent(EventItem reservedEvent) {
        ReservedEvent = reservedEvent;
    }

    private EventItem ReservedEvent;

    public ArrayList<ReservationTypeItem> getReservationTypeItemList() {
        return ReservationTypeItemList;
    }

    public void setReservationTypeItemList(ArrayList<ReservationTypeItem> reservationTypeItemList) {
        ReservationTypeItemList = reservationTypeItemList;
    }

    private ArrayList<ReservationTypeItem> ReservationTypeItemList;

    private Constants() {

        //ToDo here

    }

    public static Constants getInstance() {
        if (con == null) {
            con = new Constants();
        }
        return con;
    }
}
