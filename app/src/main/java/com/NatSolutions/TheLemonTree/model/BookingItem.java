package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Hagar on 5/27/2017.
 */

public class BookingItem  implements Serializable{
    @SerializedName("ReservationId")
    public String ReservationId;
    @SerializedName("VenueName")
    public String VenueName;
    @SerializedName("ReservationDate")
    public String ReservationDate;
    @SerializedName("ReservationType")
    public String ReservationType;
    @SerializedName("NumberOfPeople")
    public String NumberOfPeople;
    @SerializedName("DepositAmount")
    public String DepositAmount;
    @SerializedName("TableNumber")
    public String TableNumber;
    @SerializedName("TimeSlot")
    public String TimeSlot;
    @SerializedName("BarcodeSerial")
    public String BarcodeSerial;
    @SerializedName("BarcodeImagePath")
    public String BarcodeImagePath;
    @SerializedName("Confirmed")
    public String Confirmed;
  @SerializedName("Cancelled")
    public String Cancelled;
    @SerializedName("DepositPayed")
    public String DepositPayed;

}
