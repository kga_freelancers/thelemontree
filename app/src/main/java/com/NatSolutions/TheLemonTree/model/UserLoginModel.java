package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/3/2017.
 */

public class UserLoginModel {


    public String getClientId() {
        return ClientId;
    }

    public void setClientId(String clientId) {
        ClientId = clientId;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    @SerializedName("ClientName")
    public String ClientName;
    @SerializedName("ClientId")
    public String ClientId;
    @SerializedName("ClientEmail")
    public String ClientEmail;
    @SerializedName("ClientBrithdate")
    public String ClientBrithdate;
    @SerializedName("ClientPhoneNumber")
    public String ClientPhoneNumber;
    @SerializedName("ClientGender")
    public String ClientGender;
   @SerializedName("ClientAddress")
    public String ClientAddress;

}
