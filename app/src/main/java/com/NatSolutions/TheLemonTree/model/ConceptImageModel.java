package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ConceptImageModel {
    @SerializedName("Image")
    public String Image;
}
