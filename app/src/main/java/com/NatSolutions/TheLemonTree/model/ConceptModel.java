package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ConceptModel {
    @SerializedName("ConceptId")
    public String ConceptId;
    @SerializedName("ConceptName")
    public String ConceptName;
    @SerializedName("ConceptDescription")
    public String ConceptDescription;
    @SerializedName("ConceptIsactive")
    public boolean ConceptIsactive;
    @SerializedName("ConceptImages")
    public ArrayList<ConceptImageModel> ConceptImages;
    public int conceptImage;

}
