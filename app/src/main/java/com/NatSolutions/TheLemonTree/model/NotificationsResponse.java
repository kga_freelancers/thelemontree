package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amr on 23/09/17.
 */

public class NotificationsResponse {
    @SerializedName("Obj")
    public List<NotificationModel> notificationModelList;

}
