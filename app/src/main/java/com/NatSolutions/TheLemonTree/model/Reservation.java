package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Hagar on 6/18/2017.
 */

public class Reservation {
    @SerializedName("ReservationInfo")
    public ArrayList<ReservationDetailsItem> ReservationInfo;
}
