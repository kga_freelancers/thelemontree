package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/11/2017.
 */

public class ReservationTypeItem {
    @SerializedName("ReservationTypeName")
    private String ReservationTypeName;
    @SerializedName("ReservationTypeId")
    private String ReservationTypeId;

    public String getReservationTypeName() {
        return ReservationTypeName;
    }

    public void setReservationTypeName(String reservationTypeName) {
        ReservationTypeName = reservationTypeName;
    }

    public String getReservationTypeId() {
        return ReservationTypeId;
    }

    public void setReservationTypeId(String reservationTypeId) {
        ReservationTypeId = reservationTypeId;
    }

    public ArrayList<ReservationTypeTimeSoltsItem> getReservationTypeTimeSolts() {
        return ReservationTypeTimeSolts;
    }

    public void setReservationTypeTimeSolts(ArrayList<ReservationTypeTimeSoltsItem> reservationTypeTimeSolts) {
        ReservationTypeTimeSolts = reservationTypeTimeSolts;
    }

    @SerializedName("ReservationTypeTimeSolts")
    private ArrayList<ReservationTypeTimeSoltsItem> ReservationTypeTimeSolts;


}
