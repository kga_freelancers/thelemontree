package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by karim on 15/07/17.
 */

public class NotificationModel {

    @SerializedName("NotificationMessage")
    public String message;
    @SerializedName("NotificationDate")
    public String date;

}
