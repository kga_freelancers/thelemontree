package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/12/2017.
 */

public class GuestList {
    @SerializedName("GuestName")
    public String GuestName;
    @SerializedName("GuestPhoneNumber")
    public String GuestPhoneNumber;

}
