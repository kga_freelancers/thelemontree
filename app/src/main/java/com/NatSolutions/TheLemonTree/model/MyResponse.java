package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by karim on 3/3/18.
 */

public class MyResponse {
    @SerializedName("Message")
    public String message;
    @SerializedName("Status")
    public boolean status;
    @SerializedName("Obj")
    public Obj obj;


    public class Obj {
    }
}
