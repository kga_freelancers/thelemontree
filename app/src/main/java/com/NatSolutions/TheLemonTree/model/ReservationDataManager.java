package com.NatSolutions.TheLemonTree.model;

import android.content.Context;

import com.NatSolutions.TheLemonTree.base.BaseCloudDataManager;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;

import org.json.JSONException;

/**
 * Created by Rowan Tarek on 29/4/2017
 */

public class ReservationDataManager {
    //=================================================================//
    private final String GetAllVenues_URL = "Venue/GetAllVenues";
    private final String CheckReservationBookingDateHasEvent_URL = "Booking/CheckReservationBookingDateHasEvent";
    private final String CreatetReservationTypeAndTimeSlotsByVenueId_URL = "Booking/GetReservationTypeAndTimeSlotsByVenueId";
    private final String CreatetNewReservation_URL = "Booking/BookNewReservation";

    //=================================================================//
    public void GetAllVenues(Context context, Object object,
                                     OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJson(GetAllVenues_URL, listener, null);
    }//end registerWithFacebook
    // =================================================================//



    //=================================================================//
    public void CheckReservationBookingDateHasEvent(Context context, String date,String id,
                                     OnRequestCompletedListener listener) throws JSONException {


        BaseCloudDataManager.getInstance(context).requestJson(CheckReservationBookingDateHasEvent_URL+"?date="+date+"&venueId="+id, listener,null);
    }//end registerWithFacebook
    // =================================================================//


    //=================================================================//
    public void CreatetReservationTypeAndTimeSlotsByVenueId(Context context,String id,
                                                    OnRequestCompletedListener listener) throws JSONException {


        BaseCloudDataManager.getInstance(context).requestJson(CreatetReservationTypeAndTimeSlotsByVenueId_URL+"?venueId="+id, listener,null);
    }//end registerWithFacebook
    // =================================================================//

    public void createNewReservation(Context context, String obj,String clientid,String venueId,String reservationid,String timeslotId,String reservationDate,String numberOfPeople,String notes,
                                     OnRequestCompletedListener listener) throws JSONException {

        BaseCloudDataManager.getInstance(context).requestJsonPostAerray(CreatetNewReservation_URL+"?ClientId="+clientid+"&VenueId="+venueId+"&ReservationTypeId="+reservationid+"&TimeSlotId="+timeslotId+"&ReservationDate="+reservationDate+"&NumberOfPeople="+numberOfPeople+"&Notes="+notes, listener, obj);
    }//end registerWithFacebook
}//end UserAccountDataManager
