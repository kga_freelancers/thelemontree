package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;


public class RegisterFacebookUserModel {
    //=================================================================//
    @SerializedName("ClientFacebookId")
    private String facebookId;
    @SerializedName("ClientName")
    private String name;
    @SerializedName("ClientGender")
    private String gender;
    @SerializedName("ClientBrithdate")
    private String birthDate;
    @SerializedName("ClientPhotoPath")
    private String imageUrl;
    @SerializedName("ClientPhoneNumber")
    private String phoneNumber;
    @SerializedName("ClientEmail")
    private String emailAddress;
    @SerializedName("ClientAddress")
    private String address;
    @SerializedName("ClientFacebookProfile")
    private String facebookProfile;  //don't know what this is and how it is supposed to be used


    //=================================================================//

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setFacebookProfile(String facebookProfile) {
        this.facebookProfile = facebookProfile;
    }
    //=================================================================//
    public String getFacebookId() {
        return facebookId;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getAddress() {
        return address;
    }

    public String getFacebookProfile() {
        return facebookProfile;
    }

    //=================================================================//
}//end
