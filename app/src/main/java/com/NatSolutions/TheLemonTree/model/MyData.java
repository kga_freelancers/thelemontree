package com.NatSolutions.TheLemonTree.model;


import com.NatSolutions.TheLemonTree.R;

public class MyData {

    public static String[] nameArray = {"concepts", "reservations", "music", "blog", "boutique", "about us"};
    public static String[] descriptionArray = {"", "", "", "", "", ""};
    //""Rituals by The Lemon Tree & Co.", by The Lemon Tree & Co.",
    public static String[] conceptsTitleArray = {"the lemon tree & co. katameya",
            "the lemon tree & co. marassi",
            "the beach bar by The lemon tree & co.",
            "family & friends",
            "club clandestine"};
    //"A secluded private oasis that offers guests a chance to enjoy the perfect mix of sun, sea and sand whilst delving on The Lemon Tree & Co’s signature cuisine and refreshing cocktails"
    public static String[] conceptsDescripArray = {"drawing inspiration from the authenticity of the uniquely designed clubhouse space. the season driven chameleon combines an upscale dining experience along with carefully selected entertainment to complete the sensational experience.\n" +
            "the culinary team continues to build on its strategy of working closely with partners that provide the best local and international produce, promising the delivery of the seasons’ freshest ingredients to our guests’ tables.\n"

            , "this world engages your every sense leaving you with not a moment of dulness throughout the night. you are taken on a gastronomical trip to the mediterranean with the exquisite food and amazing delicacies that are on offer.\n" +
            "it is a place where music gets louder, drinks get cooler and lights are darker, a place where you stand, shine and dance the night away.\n"

            , "located in telal on the stunning coast of sidi abd el rahman, facing crystal blue waters and ivory white sand. the beach Bar has something for everyone, be it a fulfilling quick lunch after a long day at the beach or even a quiet group dinner under a sea of stars. the experience of this beach front breezy restaurant is a keystone to a complete summer experience."

            , "founded by the lemon tree & co., family & friends has transformed from a weekly club night to a club concept. \n" +
            "what separates Family & Friends from all other nights is that it was designed specifically for our guests; it is a night dedicated to them. Family & Friends was born from the demand for a sophisticated, elegant and familiar environment for those who wish to indulge in the deeper side of music without having to delve into the underground scene.\n"

            , "a discrete supper club that combines mystery and finesse. a maximum of 100 special guests are invited to a secret and unexpected location to experience the most intriguing 3 hours of a fine dining journey. \n" +
            "the challenging role of turning the most unconventional location into a restaurant for the night/ day is what defines the team’s determination to providing an unforgettable inclusive experience. Whether it is a houseboat, an island in the middle of the River Nile, or even a concept store, club clandestine is a chameleon that shapes up its identity in harmony to its surroundings. \n" +
            "the boutique concept served as a star project on all levels, challenging the lemon tree’s hospitality, culinary and creative teams to maximize their work efficiency channeling the best of their capabilities towards granting their esteemed guests an evening to remember. The concept is based on trust between invitees and the brand showing how a surprise 6-course fine dining experience can conclude to be a fully sensational getaway from the usual.\n"};

    //"Marassi, El Alamein, North coast."
    public static String[] conceptsAddress = {"club house, katameya heights, new cairo.", "club house, marassi, el alamein, north coast.", "telal, el alamein, north coast.", null, null};
    public static String[] conceptsLatlang = {"the lemon tree co.katameya", "marassi beachclub house", "the+lemon+tree", null, null};
    public static String[] conceptsLatitude = {"29.9950643", "30.977236", "31.0142256", null, null};
    public static String[] conceptsLongitude = {"31.4025865", "28.756584", "28.6089154", null, null};
//R.drawable.three,

    public static Integer[] conceptsDrawableArray = {R.drawable.one, R.drawable.two,
            R.drawable.four, R.drawable.five, R.drawable.club_clandestine};
    public static Integer[] drawableArray = {R.drawable.concept, R.drawable.res, R.drawable.music,
            R.drawable.blog, R.drawable.botique,
            R.drawable.botique};
}
