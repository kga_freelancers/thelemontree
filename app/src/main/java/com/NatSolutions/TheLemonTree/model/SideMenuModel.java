package com.NatSolutions.TheLemonTree.model;

/**
 * Created by amr on 24/08/17.
 */

public class SideMenuModel {
    public String name;
    public int image;

    public SideMenuModel(String name, int image) {
        this.name = name;
        this.image = image;
    }
}
