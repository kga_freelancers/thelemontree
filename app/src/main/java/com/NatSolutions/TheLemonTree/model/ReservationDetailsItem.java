package com.NatSolutions.TheLemonTree.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ReservationDetailsItem {

    @SerializedName("ClientPhoneNumber")
    public String ClientPhoneNumber;
    @SerializedName("ReservationTypeName")
    public String ReservationTypeName;
    @SerializedName("TimeSlotValue")
    public String TimeSlotValue;
    @SerializedName("VenueName")
    public String VenueName;
    @SerializedName("Notes")
    public String Notes;
    @SerializedName("NumberOfPeople")
    public String PeopleAlreadyAttend;
    @SerializedName("BarcodeImagePath")
    public String BarcodeImagePath;
    @SerializedName("BarcodeSerial")
    public String BarcodeSerial;
    @SerializedName("ReservationDate")
    public String ReservationDate;


}
