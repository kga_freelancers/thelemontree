package com.NatSolutions.TheLemonTree.utils;

import android.graphics.Typeface;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;

import java.util.Locale;

/**
 * Created by karim on 04/09/16.
 */
public class FontsUtils {
    private static Typeface dinarTypeFace, ubuntuTypeFace, returnedTypeFace;

    public static Typeface getDinarTypeFace() {

        if (dinarTypeFace == null && isArabic()) {
            dinarTypeFace = Typeface.createFromAsset(LemonTreeApp.getInstance().getAssets(), "fonts/lm_regular.ttf");
            returnedTypeFace = dinarTypeFace;
        } else if (ubuntuTypeFace == null && !(isArabic())) {
            ubuntuTypeFace = Typeface.createFromAsset(LemonTreeApp.getInstance().getAssets(), "fonts/lm_regular.ttf");
            returnedTypeFace = ubuntuTypeFace;

        }
        return returnedTypeFace;

    }


    public static void setTypeFace(View view , String fontName) {
        if (view instanceof TextView)
            ((TextView) view).setTypeface(getTypeFace(fontName));

    }

    public static void setTypeFace(ViewGroup views) {
        for (int i = views.getChildCount() - 1; i >= 0; i--) {
            View child = views.getChildAt(i);
            if (child instanceof ViewGroup) {
                setTypeFace((ViewGroup) child);
            } else {
                if (child != null) {
                    if (child instanceof TextView) {
                        ((TextView) child).setTypeface(getDinarTypeFace());
                    }
                }
            }
        }

    }

    private static Typeface lmBoldTypeFace, lmRegularTypeFace, returnTypeFace;

    public static Typeface getTypeFace(String fontName) {

        switch (fontName) {
            case Constants.LEMON_TREE_BOLD:
                lmBoldTypeFace = Typeface.createFromAsset(LemonTreeApp.getInstance().getAssets(), "AlegreyaSansSC-Bold.ttf");
                returnTypeFace = lmBoldTypeFace;
                break;
            case Constants.LEMON_TREE_REGULAR:
                lmRegularTypeFace = Typeface.createFromAsset(LemonTreeApp.getInstance().getAssets(), "AlegreyaSansSC-Regular.ttf");
                returnTypeFace = lmRegularTypeFace;
                break;


        }

        return returnTypeFace;

    }


    public static void setTypeFaceForSubViews(ViewGroup views, String fontName) {
        for (int i = views.getChildCount() - 1; i >= 0; i--) {
            View child = views.getChildAt(i);
            if (child instanceof ViewGroup) {
                setTypeFaceForSubViews((ViewGroup) child, fontName);
            } else {
                if (child != null) {
                    if (child instanceof TextView) {
                        ((TextView) child).setTypeface(getTypeFace(fontName));
                    }
                }
            }
        }

    }


    public static boolean isArabic() {
        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = LemonTreeApp.getInstance().getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = LemonTreeApp.getInstance().getResources().getConfiguration().locale;
        }
        // Locale current = App.getContext().getResources().getConfiguration().locale;
        if (locale.toString().contains("ar")) return true;
        else return false;
    }
}
