package com.NatSolutions.TheLemonTree.utils;

/**
 * Created by karim on 8/30/17.
 */

public class MessageEvent {

    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}