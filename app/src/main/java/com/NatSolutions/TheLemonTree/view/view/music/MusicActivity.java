package com.NatSolutions.TheLemonTree.view.view.music;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.ConnectionManager;
import com.NatSolutions.TheLemonTree.model.MyResponse;
import com.NatSolutions.TheLemonTree.model.Track;
import com.NatSolutions.TheLemonTree.utils.Utils;
import com.NatSolutions.TheLemonTree.view.adapters.TrackPagerAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.example.jean.jcplayer.JcAudio;
import com.example.jean.jcplayer.JcAudioPlayer;
import com.example.jean.jcplayer.JcPlayerView;
import com.example.jean.jcplayer.JcStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_TRACK;
import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_TRACK_LIST;

/**
 * Created by Hagar on 5/26/2017.
 */

public class MusicActivity extends AppCompatActivity implements JcPlayerView.OnInvalidPathListener
        , JcPlayerView.JcPlayerViewStatusListener, JcAudioPlayer.controlPLaying {

    ViewPager mViewPager;
    TrackPagerAdapter trackPagerAdapter;
    List<Track> mTrackList = new ArrayList<>();
    Track mTrack;
    private JcPlayerView player;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        player = (JcPlayerView) findViewById(R.id.player);
        player.getInstance(this);
        mTrackList = new Gson().fromJson(getIntent().getStringExtra(EXTRA_TRACK_LIST), new TypeToken<List<Track>>() {
        }.getType());
        mTrack = new Gson().fromJson(getIntent().getStringExtra(EXTRA_TRACK), Track.class);
        setViewPagerData(mTrackList);

        List<JcAudio> jcAudios = new ArrayList<>();
        for (Track track : mTrackList)
            jcAudios.add(JcAudio.createFromURL(track.trackName + "(" + track.trackArtist + ")"
                            + "\n" + "\n" + track.trckDescription
                    , track.trckPath));

        player.initPlaylist(jcAudios);
        player.registerInvalidPathListener(this);
        player.registerStatusListener(this);
        JcAudioPlayer.getInstance().setListener(this);


    }

    private void setViewPagerData(final List<Track> trackList) {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        trackPagerAdapter = new TrackPagerAdapter(getSupportFragmentManager(), trackList);
        mViewPager.setAdapter(trackPagerAdapter);
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(((width - getResources().getDimensionPixelSize(R.dimen.item_width)) / 2), 0, ((width - getResources().getDimensionPixelSize(R.dimen.item_width)) / 2), 0);

        int idx = -1;
        for (int i = 0; i < trackList.size(); i++) {
            if (trackList.get(i).trackId == mTrack.trackId) {
                idx = i;
                break;
            }
        }
        mViewPager.setCurrentItem(idx);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                player.playAudio(player.getMyPlaylist().get(position));
                mViewPager.setCurrentItem(position);

//                if (position == trackPagerAdapter.getCount() - 1) {
//                    mViewPager.setCurrentItem(position);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.createNotification();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.kill();
    }

    @Override
    public void onPathError(JcAudio jcAudio) {
        Toast.makeText(this, jcAudio.getPath() + "With problems", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPausedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPlayingStatus(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChangedStatus(JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onCompletedAudioStatus(JcStatus jcStatus) {
        updateProgress(jcStatus);
    }

    @Override
    public void onPreparedAudioStatus(JcStatus jcStatus) {

    }

    private void updateProgress(final JcStatus jcStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                float progress = (float) (jcStatus.getDuration() - jcStatus.getCurrentPosition())
                        / (float) jcStatus.getDuration();
                progress = 1.0f - progress;
            }
        });
    }


    @Override
    public void onNextClicked(int trackPosition) {
        player.playAudio(player.getMyPlaylist().get(trackPosition));
        mViewPager.setCurrentItem(trackPosition);

    }

    @Override
    public void onPreviousClicked(int trackPosition) {
        player.playAudio(player.getMyPlaylist().get(trackPosition));
        mViewPager.setCurrentItem(trackPosition);
    }

    @Override
    public void onFavoriteClicked(int trackPosition) {
        Call<MyResponse> call = ConnectionManager.getApiManager().addFavoriteTrack(mTrackList.get(trackPosition).trackId,
                Integer.parseInt(SharedPreferencesManager.getInstance(this).getString(getResources()
                        .getString(R.string.user_logged_acess_pref))));
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Utils.showPopUp("Track added to favorite list", MusicActivity.this);
                }
                else{
                    Utils.showPopUp("Track already in your favorite list!", MusicActivity.this);
                }
            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Utils.showPopUp("something went wrong", MusicActivity.this);
            }
        });
    }

}