package com.NatSolutions.TheLemonTree.view.view.concepts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.AppDataManager;
import com.NatSolutions.TheLemonTree.model.ConceptModel;
import com.NatSolutions.TheLemonTree.model.DataModel;
import com.NatSolutions.TheLemonTree.model.MyData;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.adapters.SkewAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/26/2017.
 */

public class ConceptsListActivity extends AppCompatActivity implements OnRequestCompletedListener {


    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<ConceptModel> data;
    public static View.OnClickListener myOnClickListener;
    private NetworkUtil network;
    OnRequestCompletedListener listener;
    private TextView title;
    private Typeface regular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concepts);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        title = (TextView) findViewById(R.id.title);
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        title.setText("concepts");
        title.setTypeface(regular);
        recyclerView.setHasFixedSize(true);
        network = new NetworkUtil();
        listener = this;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        myOnClickListener = new ConceptsListActivity.MyOnClickListener(this);

        if (network.isInternetOn(ConceptsListActivity.this)) {
            LemonTreeApp.getInstance().showDialog(ConceptsListActivity.this);
            AppDataManager dataManager = new AppDataManager();
            try {
                dataManager.GetAllConcepts(this, listener);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");

        }


    }

    @Override
    public void onSuccess(String message, Object response) {

    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {
        LemonTreeApp.getInstance().hideDialog();

        data = new ArrayList(response.length());
        ArrayList<DataModel> conceptList = new ArrayList<DataModel>();

        for (int i = 0; i < response.length(); i++) {
            try {
                data.add(new Gson().fromJson(response.getJSONObject(i).toString(), ConceptModel.class));
                String description = "", name = "";
                if (data.get(i).ConceptDescription != null && data.get(i).ConceptDescription.length() >= 10) {
                    description = data.get(i).ConceptDescription.substring(0, 10);
                } else {
                    description = data.get(i).ConceptDescription;
                }
                if (data.get(i).ConceptName != null && data.get(i).ConceptName.length() >= 10) {
                    name = data.get(i).ConceptName.substring(0, 10);
                } else {
                    name = data.get(i).ConceptName;
                }
                DataModel d = new DataModel(name, description, MyData.drawableArray[i]);
                conceptList.add(d);
            } catch (Exception e) {
                Log.e("", "" + e);
            }
        }


        adapter = new SkewAdapter(conceptList, myOnClickListener);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFail(String ex) {

    }

    private static class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            int itemPosition = recyclerView.getChildLayoutPosition(v);
            LemonTreeApp.getInstance().setSelectedConcept(data.get(itemPosition));
            UIManager.startConceptDetails((Activity) context);
        }
    }

}
