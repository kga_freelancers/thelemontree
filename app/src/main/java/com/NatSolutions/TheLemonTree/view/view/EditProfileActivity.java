package com.NatSolutions.TheLemonTree.view.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.ProfileItem;
import com.NatSolutions.TheLemonTree.model.UserAccountDataManager;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Hagar on 5/27/2017.
 */

public class EditProfileActivity extends Activity implements OnRequestCompletedListener {
    private EditText nameEdit, phoneEdit, birthEdit, genderEdit, emailEdit, addressEditText;
    private CircleImageView profileImage;
    String userName, userFacebookId, userAddress = "", userBirthdate, userEmail, userProfile, userGender, userPhone;
    private Button registerBtn;
    private String url;
    private TextView welcomTxt, nameTxt, birthTxt, genderTxt, emailTxt, phoneTxt, addressTextView, toolbarTitleTextView;
    private Typeface black, regular, bold;
    private NetworkUtil network;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        network = new NetworkUtil();

        userBirthdate = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_birthdate_pref), null);
        userAddress = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_address_pref), "");
        userEmail = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_email_pref), null);
        userName = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_name_pref), null);
        userGender = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_gender_pref), null);
        userPhone = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_phone_pref), null);
        userFacebookId = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookid_pref), null);
        userProfile = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookprofile_pref), null);
        nameEdit = (EditText) findViewById(R.id.user_name_edit);
        nameEdit.setEnabled(false);
        nameEdit.setClickable(false);
        phoneEdit = (EditText) findViewById(R.id.mobile_number_editText);
        birthEdit = (EditText) findViewById(R.id.birth_edit);
        toolbarTitleTextView = (TextView) findViewById(R.id.toolbar_title);

        addressEditText = (EditText) findViewById(R.id.address_editText);
        addressTextView = (TextView) findViewById(R.id.address_textView);

        birthEdit.setEnabled(false);
        birthEdit.setClickable(false);
        genderEdit = (EditText) findViewById(R.id.gender_edit);
        genderEdit.setEnabled(false);
        genderEdit.setClickable(false);
        emailEdit = (EditText) findViewById(R.id.email_edit);
        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        registerBtn = (Button) findViewById(R.id.next_register);
        nameTxt = (TextView) findViewById(R.id.name_txt);
        emailTxt = (TextView) findViewById(R.id.email_txt);
        phoneTxt = (TextView) findViewById(R.id.mobile_number_textView);
        genderTxt = (TextView) findViewById(R.id.gender_txt);
        birthTxt = (TextView) findViewById(R.id.birth_txt);
        welcomTxt = (TextView) findViewById(R.id.welcome_txt);
        welcomTxt.setVisibility(View.INVISIBLE);


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });
        applyFontStyle();
        showUserData();
    }

    private void register() {

        ProfileItem user = new ProfileItem();
        if (emailEdit.getText().length() > 0)
            user.ClientEmail = emailEdit.getText().toString();
        else
            user.ClientEmail = "";


        if (phoneEdit.getText().length() > 0)
            user.ClientPhoneNumber = phoneEdit.getText().toString();
        else
            user.ClientPhoneNumber = "";

        user.ClientId = SharedPreferencesManager.getInstance(EditProfileActivity.this).getString(getResources().getString(R.string.user_logged_acess_pref));
//        user.ClientId="1020";
        if (network.isInternetOn(EditProfileActivity.this)) {

            UserAccountDataManager dataManager = new UserAccountDataManager();
            try {

                dataManager.UpdateUserProfile(EditProfileActivity.this, new Gson().toJson(user), this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");
        }

    }

    private void showUserData() {


        if (userName != null) {
            nameEdit.setText(userName);
        }
        if (userBirthdate != null) {
            birthEdit.setText(userBirthdate);
        }
        if (userEmail != null) {
            emailEdit.setText(userEmail);
        }
        if (userGender != null) {
            genderEdit.setText(userGender);
        }
        if (userPhone != null) {
            phoneEdit.setText(userPhone);
        }
        if (userFacebookId != null) {
            url = "https://graph.facebook.com/" + userFacebookId + "/picture?type=large";

            Picasso.with(EditProfileActivity.this)
                    .load(url)
                    .placeholder(R.drawable.profile_pic_bg)
                    .error(R.drawable.profile_pic_bg)
                    .into(profileImage);
        }
    }

    //=================================================================//
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    //=================================================================//
    @Override
    public void onSuccess(String m, Object response) {
        SharedPreferencesManager.getInstance(EditProfileActivity.this).setString(getResources().getString(R.string.user_email_pref), emailEdit.getText().toString());
        SharedPreferencesManager.getInstance(EditProfileActivity.this).setString(getResources().getString(R.string.user_phone_pref), phoneEdit.getText().toString());
        String sResponse = response.toString();
//        ObjectMapper m = new ObjectMapper();
//        LoginResponse response = m.readValue(b.toString(), LoginResponse.class);
//        LemonTreeApp.getInstance().showToast(m);
        finish();
    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {

    }

    @Override
    public void onFail(String ex) {
        Toast.makeText(this, ex, Toast.LENGTH_LONG).show();
    }

    private void applyFontStyle() {
        black = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Black.ttf");
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        welcomTxt.setTypeface(regular);
        nameEdit.setTypeface(regular);
        nameTxt.setTypeface(bold);
        emailEdit.setTypeface(regular);
        emailTxt.setTypeface(bold);
        genderEdit.setTypeface(regular);
        genderTxt.setTypeface(bold);
        phoneEdit.setTypeface(regular);
        phoneTxt.setTypeface(bold);
        birthEdit.setTypeface(regular);
        birthTxt.setTypeface(bold);
        registerBtn.setText("update");
        registerBtn.setTypeface(regular);
        addressEditText.setTypeface(regular);
        addressTextView.setTypeface(bold);
        toolbarTitleTextView.setTypeface(bold);

    }


}
