package com.NatSolutions.TheLemonTree.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.ConnectionManager;
import com.NatSolutions.TheLemonTree.model.TracksResponse;
import com.NatSolutions.TheLemonTree.view.adapters.MusicListAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MusicListActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private MusicListAdapter musicListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        initializeRecyclerView();
        getAllTracks();

    }

    private void initializeRecyclerView() {
        musicListAdapter = new MusicListAdapter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(musicListAdapter);
    }
    private void getAllTracks() {
        progressBar.setVisibility(View.VISIBLE);
        Call<TracksResponse> call = ConnectionManager.getApiManager()
                .getAllTracks();
        call.enqueue(new Callback<TracksResponse>() {
            @Override
            public void onResponse(Call<TracksResponse> call, Response<TracksResponse> response) {
                if (response.body() != null) {
                    musicListAdapter.setData(response.body().response.trackList);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<TracksResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(MusicListActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        });
    }
}

