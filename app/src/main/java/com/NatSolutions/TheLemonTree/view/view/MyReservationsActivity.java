package com.NatSolutions.TheLemonTree.view.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.AppDataManager;
import com.NatSolutions.TheLemonTree.model.BookingItem;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.DataModel;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.view.adapters.MyReservationAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MyReservationsActivity extends AppCompatActivity implements OnRequestCompletedListener {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<BookingItem> data;
    //private ProgressBar progressBar;
    private NetworkUtil network;
    OnRequestCompletedListener listener;
    private TextView title;
    private Typeface regular;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private RelativeLayout mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_reservations);
        //  View view = inflater.inflate(R.layout.booking_fragment, container, false);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        toolbarTitle.setText("my reservations");
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        //   progressBar = (ProgressBar) findViewById(R.id.progressBar);

        recyclerView.setHasFixedSize(true);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
//        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_dashed));
//        recyclerView.addItemDecoration(dividerItemDecoration);
        network = new NetworkUtil();
        listener = this;
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (network.isInternetOn(this)) {
            LemonTreeApp.getInstance().showDialog(this);
            AppDataManager dataManager = new AppDataManager();
            try {
                dataManager.GetUserBookings(this, SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_logged_acess_pref)), listener);
//                dataManager.GetUserBookings(getActivity(),"1020", listener);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");

        }

        FontsUtils.setTypeFace(toolbarTitle, Constants.LEMON_TREE_REGULAR);

    }


    @Override
    public void onSuccess(String message, Object response) {

    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {
        LemonTreeApp.getInstance().hideDialog();

        data = new ArrayList(response.length());
        ArrayList<DataModel> conceptList = new ArrayList<DataModel>();

        for (int i = 0; i < response.length(); i++) {
            try {
                data.add(new Gson().fromJson(response.getJSONObject(i).toString(), BookingItem.class));
                String description = "", name = "";

            } catch (Exception e) {
                Log.e("", "" + e);
            }
        }


        adapter = new MyReservationAdapter(data, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFail(String ex) {
        Toast.makeText(this, "Something went Wrong", Toast.LENGTH_LONG);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(this, HomeActivityNew.class);
        startActivity(intent);
    }
}
