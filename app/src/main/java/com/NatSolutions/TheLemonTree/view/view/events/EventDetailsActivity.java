package com.NatSolutions.TheLemonTree.view.view.events;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.EventItem;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Hagar on 5/26/2017.
 */

public class EventDetailsActivity extends AppCompatActivity
        implements OnMapReadyCallback {
    private SliderLayout sliderShow;
    EventItem event;
    private Button make_reservatoin;
    private TextView name, date, attendeing_number, call_number, location, location_title;
    private Typeface regular;
    ImageView call, navigate;
    private LinearLayout call_lay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_event_details);
        event = LemonTreeApp.getInstance().getSelectedEvent();
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        navigate = (ImageView) findViewById(R.id.navigate);

        name = (TextView) findViewById(R.id.event_details_name);
        name.setTypeface(regular);
        name.setText(event.EventName.toString());
//        date = (TextView) findViewById(R.id.event_details_date);
//        date.setTypeface(regular);
        if(event.EventDate!=null)
        date.setText(event.EventDate.toString());

        attendeing_number = (TextView) findViewById(R.id.event_details_attending_number);
        attendeing_number.setTypeface(regular);
        attendeing_number.setText(event.EventMaxCapacity);
//        call_number = (TextView) findViewById(R.id.call_number);
////        call_number.setText("Mobile:" + event.);
//        call_number.setTypeface(regular);

        location = (TextView) findViewById(R.id.event_details_location);
        location.setText(event.EventLocation);
        location.setTypeface(regular);

        location_title = (TextView) findViewById(R.id.location_title);
        location_title.setTypeface(regular);

        make_reservatoin = (Button) findViewById(R.id.make_reservatoin);
        make_reservatoin.setTypeface(regular);
        make_reservatoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK,returnIntent);
                finish();            }
        });
        make_reservatoin.setVisibility(View.GONE);




        sliderShow = (SliderLayout) findViewById(R.id.slider);
        for (int i = 0; i < 3; i++) {
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView
                    .image(event.EventPhotoPath);

            sliderShow.addSlider(textSliderView);
        }
        sliderShow.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderShow.setCustomAnimation(new DescriptionAnimation());
        sliderShow.setDuration(2000);
        ImageView image=(ImageView)findViewById(R.id.event_image);
        image.setImageResource(event.photoId);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        LinearLayout map_lay=(LinearLayout)findViewById(R.id.map_lyout);
        if(event.title!=null) {

            mapFragment.getMapAsync(this);
            map_lay.setVisibility(View.VISIBLE);
            map_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?"+event.title+"="+event.Latitude+","+event.Longitude+""));
                    startActivity(intent);
                }
            });
        }else
        {
            mapFragment.setUserVisibleHint(false);
            map_lay.setVisibility(View.GONE);
            location_title.setVisibility(View.GONE);

        }

        TextView expTv1 = (TextView) findViewById(R.id.expand_text_view);

// IMPORTANT - call setText on the ExpandableTextView to set the text content to display
        expTv1.setText(event.EventDescription);
        expTv1.setTypeface(regular);
    }


    @Override
    protected void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            LatLng sydney = new LatLng(Double.parseDouble(event.Latitude), Double.parseDouble(event.Longitude));
            googleMap.addMarker(new MarkerOptions().position(sydney)
                    .title(event.title));
//            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15), 2000, null);

        } catch (Exception e) {

        }
    }

}
