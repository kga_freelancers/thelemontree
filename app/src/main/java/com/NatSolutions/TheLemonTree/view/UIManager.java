package com.NatSolutions.TheLemonTree.view;

import android.app.Activity;
import android.content.Intent;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.view.view.EditProfileActivity;
import com.NatSolutions.TheLemonTree.view.view.HomeActivityNew;
import com.NatSolutions.TheLemonTree.view.view.LoginActivity;
import com.NatSolutions.TheLemonTree.view.view.MyReservationsActivity;
import com.NatSolutions.TheLemonTree.view.view.ProfileActivity;
import com.NatSolutions.TheLemonTree.view.view.RegisterationActivity;
import com.NatSolutions.TheLemonTree.view.view.aboutUs.AboutUsActivity;
import com.NatSolutions.TheLemonTree.view.view.blog.BlogActivity;
import com.NatSolutions.TheLemonTree.view.view.botique.BotiqueActivity;
import com.NatSolutions.TheLemonTree.view.view.concepts.ConceptDetailsActivity;
import com.NatSolutions.TheLemonTree.view.view.concepts.ConceptsListActivityNew;
import com.NatSolutions.TheLemonTree.view.view.events.EventDetailsActivity;
import com.NatSolutions.TheLemonTree.view.view.events.EventListActivity;
import com.NatSolutions.TheLemonTree.view.view.music.MusicActivity;
import com.NatSolutions.TheLemonTree.view.view.reservation.BarCodeActivity;
import com.NatSolutions.TheLemonTree.view.view.reservation.ReservationActivityOne;
import com.NatSolutions.TheLemonTree.view.view.reservation.ReservationActivityThree;
import com.NatSolutions.TheLemonTree.view.view.reservation.ReservationActivityTwo;
import com.NatSolutions.TheLemonTree.view.view.restaurants.ResturantDetailsActivity;
import com.NatSolutions.TheLemonTree.view.view.restaurants.ResturantsListActivity;


/**
 * Created by Hagar on 3/18/2017.
 */

public class UIManager {
    /**
     * start LOGIN SCREEN
     *
     * @param activity
     */
    public static void startlogin(Activity activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
        activity.finish();

    }

    /**
     * start LOGIN SCREEN
     *
     * @param activity
     */
//    public static void startProfile(Activity activity) {
//        Intent intent = new Intent(activity, ProfileActivity.class);
//        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
//    }

    public static void startMyReservationActivity(Activity activity) {
        Intent intent = new Intent(activity, MyReservationsActivity.class);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public static void startBarCodeActivity(Activity activity, String name, String type, String id) {
        Intent intent = new Intent(activity, BarCodeActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("id", id);
        intent.putExtra("type", type);
        activity.startActivity(intent);
        activity.finish();
    }


    /**
     * start Main SCREEN
     *
     * @param activity
     */
    public static void startMainActivity(Activity activity) {

        Intent mainIntent = new Intent(activity, HomeActivityNew.class);
        activity.startActivity(mainIntent);
        activity.finish();

    }

    /**
     * start Sign up SCREEN
     *
     * @param activity
     */
    public static void startSignUpActivity(Activity activity) {

        Intent intent = new Intent(activity, RegisterationActivity.class);
        activity.startActivity(intent);
        activity.finish();

    }


    /**
     * start bout Us
     *
     * @param activity
     */
    public static void startAboutUs(Activity activity) {

        Intent intent = new Intent(activity, AboutUsActivity.class);
        activity.startActivity(intent);

    }


    /**
     * start Botique
     *
     * @param activity
     */
    public static void startBotique(Activity activity) {

        Intent intent = new Intent(activity, BotiqueActivity.class);
        activity.startActivity(intent);

    }

    /**
     * start Botique
     *
     * @param activity
     */
    public static void startBlog(Activity activity) {

        Intent intent = new Intent(activity, BlogActivity.class);
        activity.startActivity(intent);

    }

    /**
     * start EventList
     *
     * @param activity
     */
    public static void startEvent(Activity activity) {

        Intent intent = new Intent(activity, EventListActivity.class);
        activity.startActivity(intent);

    }


    /**
     * start EventList
     *
     * @param activity
     */
    public static void startEventDetails(Activity activity, int code) {

        Intent intent = new Intent(activity, EventDetailsActivity.class);
        activity.startActivityForResult(intent, code);

    }

    public static void startEventDetails(Activity activity) {

        Intent intent = new Intent(activity, EventDetailsActivity.class);
        activity.startActivity(intent);

    }

    /**
     * start Concepts List
     *
     * @param activity
     */
    public static void startConceptsActivity(Activity activity) {

        Intent intent = new Intent(activity, ConceptsListActivityNew.class);
        activity.startActivity(intent);

    }


    /**
     * start Restaurants list
     *
     * @param activity
     */
    public static void startRestaurants(Activity activity) {

        Intent intent = new Intent(activity, ResturantsListActivity.class);
        activity.startActivity(intent);

    }

    /**
     * start Restaurant details
     *
     * @param activity
     */
    public static void startRestaurantDetails(Activity activity, int CODE) {

        Intent intent = new Intent(activity, ResturantDetailsActivity.class);
        activity.startActivityForResult(intent, CODE);

    }

    /**
     * start Music
     *
     * @param activity
     */
    public static void startMusic(Activity activity) {

        Intent intent = new Intent(activity, MusicActivity.class);
        activity.startActivity(intent);

    }

    /**
     * start Concept Details
     *
     * @param activity
     */
    public static void startConceptDetails(Activity activity) {

        Intent intent = new Intent(activity, ConceptDetailsActivity.class);
        activity.startActivity(intent);


    }


    /**
     * start Reservation SCREEN
     *
     * @param activity
     */
    public static void startReservationActivity(Activity activity, String venue, String venueId) {

        Intent intent = new Intent(activity, ReservationActivityOne.class);
        intent.putExtra("venue", venue);
        intent.putExtra("venueId", venueId);
        activity.startActivity(intent);

    }


    public static void startReservationSecondActivity(Activity activity, int code, String venueName, String venueId, String date, String dater) {

        Intent intent = new Intent(activity, ReservationActivityTwo.class);
        activity.overridePendingTransition(R.anim.enter, R.anim.exit);

        intent.putExtra("date", date);
        intent.putExtra("venueId", venueId);
        intent.putExtra("venueName", venueName);
        intent.putExtra("dater", dater);

        activity.startActivityForResult(intent, code);

    }

    public static void startPaymentActivity(Activity activity) {

        Intent intent = new Intent(activity, ReservationActivityThree.class);
        activity.startActivity(intent);
        activity.finish();

    }

    public static void startUserProfileSettings(Activity activity) {
        Intent intent = new Intent(activity, ProfileActivity.class);
        activity.startActivity(intent);
    }

    public static void startEditProfile(Activity activity) {

        Intent intent = new Intent(activity, EditProfileActivity.class);
        activity.startActivity(intent);

    }
}
