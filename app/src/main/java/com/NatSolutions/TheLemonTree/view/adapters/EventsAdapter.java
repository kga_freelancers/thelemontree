package com.NatSolutions.TheLemonTree.view.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.EventItem;

import java.util.ArrayList;


public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.MyViewHolder> {

    private ArrayList<EventItem> dataSet;
    private Typeface regular;
    View.OnClickListener myOnClickListener;
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView eventLocation;
        TextView eventName;
        ImageView eventImage;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.eventName = (TextView) itemView.findViewById(R.id.event_name);
            this.eventLocation = (TextView) itemView.findViewById(R.id.event_location);
            this.eventImage = (ImageView) itemView.findViewById(R.id.event_image);

        }
    }

    public EventsAdapter(ArrayList<EventItem> data, View.OnClickListener myOnClickListener) {
        this.dataSet = data;
        this.myOnClickListener = myOnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_item, parent, false);
        regular = Typeface.createFromAsset(parent.getContext().getAssets(), "AlegreyaSansSC-Regular.ttf");
         context=parent.getContext();
        view.setOnClickListener(myOnClickListener);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.eventName.setText(dataSet.get(listPosition).EventName);
        holder.eventLocation.setText(dataSet.get(listPosition).EventLocation);
        holder.eventName.setTypeface(regular);
        holder.eventLocation.setTypeface(regular);
        if(!dataSet.get(listPosition).EventPhotoPath .isEmpty()) {
            Picasso.with(context)
                    .load(dataSet.get(listPosition).EventPhotoPath)
                    .into(holder.eventImage);
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
