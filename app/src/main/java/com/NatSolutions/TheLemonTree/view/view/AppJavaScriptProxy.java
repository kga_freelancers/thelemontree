package com.NatSolutions.TheLemonTree.view.view;

import android.app.Activity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

/**
 * Created by Hagar on 6/6/2017.
 */

public class AppJavaScriptProxy {
    private Activity activity = null;
    private WebView webView  = null;
    public AppJavaScriptProxy(Activity activity, WebView webview) {
        this.activity = activity;
        this.webView  = webview;
        Log.d("Test 1","in Interface");
    }

    @JavascriptInterface
    public void showMessage(final String message) {
        Log.d("from javascript", "" + message);
        final Activity theActivity = this.activity;
        final WebView theWebView = this.webView;
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!theWebView.getUrl().startsWith("file:///android_asset/www/main.html")) {
                    return;
                }
                Toast toast = Toast.makeText(
                        theActivity.getApplicationContext(),
                        message,
                        Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }
}