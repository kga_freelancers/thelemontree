package com.NatSolutions.TheLemonTree.view.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.RegisterFacebookUserModel;
import com.NatSolutions.TheLemonTree.model.LoginResponse;
import com.NatSolutions.TheLemonTree.model.UserAccountDataManager;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by Hagar on 4/25/2017.
 */

public class RegisterationActivity extends Activity implements OnRequestCompletedListener {
    private EditText nameEdit, phoneEdit, birthEdit, genderEdit, emailEdit, addressEditText;
    private CircleImageView profileImage;
    String userName, userFacebookId, userAddress = "", userBirthdate, userEmail, userProfile, userGender, userPhone;
    private Button registerBtn;
    private String url;
    private TextView welcomTxt, nameTxt, birthTxt, genderTxt, emailTxt, phoneTxt, addressTextView;
    private Typeface black, regular, bold;
    private NetworkUtil network;

    //=================================================================//
    //=================================================================//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        network = new NetworkUtil();

        initializeViews();
        showUserData();
        applyFontStyle();

        emailEdit.setEnabled(false);
        emailEdit.setAlpha(0.5f);

        emailTxt.setText(Html.fromHtml("email " + "<font color='#EE0000'>" + "*" + "</font>"));
        phoneTxt.setText(Html.fromHtml("mobile number " + "<font color='#EE0000'>" + "*" + "</font>"));


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

    }

    private void register() {

        RegisterFacebookUserModel user = new RegisterFacebookUserModel();
        if (emailEdit.getText().length() > 0)
            user.setEmailAddress(emailEdit.getText().toString());
        else
            user.setEmailAddress("");

        if (nameEdit.getText().length() > 0)
            user.setName(nameEdit.getText().toString());
        else
            user.setName("");

        if (phoneEdit.getText().length() > 0)
            user.setPhoneNumber(phoneEdit.getText().toString());
        else
            user.setPhoneNumber("");

        if (addressEditText.getText().toString().length() > 0) {
            user.setAddress(addressEditText.getText().toString());
        } else
            user.setAddress("");


        if (genderEdit.getText().length() > 0) {
            if (genderEdit.getText().toString().toLowerCase().contains("femal")) {
                user.setGender("f");

            } else {
                user.setGender("m");

            }
        } else
            user.setGender("");

        if (birthEdit.getText().length() > 0)
            user.setBirthDate(birthEdit.getText().toString());
        else
            user.setBirthDate("");

        if (userFacebookId != null) {
            user.setFacebookId(userFacebookId);
            user.setImageUrl(url);
        }

        if (userProfile != null)
            user.setFacebookProfile(userProfile);
        if (phoneEdit.getText().toString().length() > 1) {
            if (network.isInternetOn(RegisterationActivity.this)) {

                UserAccountDataManager dataManager = new UserAccountDataManager();
                try {

                    dataManager.registerWithFacebook(RegisterationActivity.this, new Gson().toJson(user), this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                LemonTreeApp.getInstance().showToast("Network Error");
            }
        } else {
            Toast.makeText(RegisterationActivity.this, "You need To Enter your Phone Number", Toast.LENGTH_LONG).show();
        }

    }

    private void showUserData() {


        if (userName != null) {
            nameEdit.setText(userName);
        }
        if (userBirthdate != null) {
            birthEdit.setText(userBirthdate);
        }
        if (userEmail != null) {
            emailEdit.setText(userEmail);
        }
        if (userGender != null) {
            genderEdit.setText(userGender);
        }
        if (userPhone != null) {
            phoneEdit.setText(userPhone);
        }
        if (userFacebookId != null) {
            url = "https://graph.facebook.com/" + userFacebookId + "/picture?type=large";

            Picasso.with(RegisterationActivity.this)
                    .load(url)
                    .placeholder(R.drawable.profile_pic_bg)
                    .error(R.drawable.profile_pic_bg)
                    .into(profileImage);
        }
    }

    //=================================================================//
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    //=================================================================//
    @Override
    public void onSuccess(String m, Object response) {
        String sResponse = response.toString();
//        ObjectMapper m = new ObjectMapper();
//        LoginResponse response = m.readValue(b.toString(), LoginResponse.class);
        Gson gson = new Gson();
        LoginResponse registeration = gson.fromJson(sResponse, LoginResponse.class);
        if (registeration.getStatus().equalsIgnoreCase("true")) {

            if (registeration.getObj() != null && registeration.getObj().getClientId() != null) {
                SharedPreferencesManager.getInstance(this).setBoolean(getResources().getString(R.string.user_logged_pref), true);
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_logged_acess_pref), registeration.getObj().getClientId());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_phone_pref), phoneEdit.getText().toString());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_gender_pref), genderEdit.getText().toString());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_name_pref), nameEdit.getText().toString());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_email_pref), emailEdit.getText().toString());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_address_pref), addressEditText.getText().toString());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_birthdate_pref), birthEdit.getText().toString());

                UIManager.startMainActivity(RegisterationActivity.this);
            }
        }
    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {

    }

    @Override
    public void onFail(String ex) {
        Toast.makeText(this, ex, Toast.LENGTH_LONG).show();
    }


    public void applyFontStyle() {
        black = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Black.ttf");
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        welcomTxt.setTypeface(regular);
        nameEdit.setTypeface(regular);
        nameTxt.setTypeface(bold);
        addressEditText.setTypeface(regular);
        emailEdit.setTypeface(regular);
        emailTxt.setTypeface(bold);
        addressTextView.setTypeface(bold);
        genderEdit.setTypeface(regular);
        genderTxt.setTypeface(bold);
        phoneEdit.setTypeface(regular);
        phoneTxt.setTypeface(bold);
        birthEdit.setTypeface(regular);
        birthTxt.setTypeface(bold);
        registerBtn.setTypeface(bold);
    }

    public void initializeViews() {
        userBirthdate = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_birthdate_pref), null);
        userAddress = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_address_pref), "");
        userEmail = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_email_pref), null);
        userName = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_name_pref), null);
        userGender = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_gender_pref), null);
        userPhone = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_phone_pref), null);
        userFacebookId = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookid_pref), null);
        userProfile = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookprofile_pref), null);
        nameEdit = (EditText) findViewById(R.id.user_name_edit);
        phoneEdit = (EditText) findViewById(R.id.mobile_number_editText);
        birthEdit = (EditText) findViewById(R.id.birth_edit);
        genderEdit = (EditText) findViewById(R.id.gender_edit);
        emailEdit = (EditText) findViewById(R.id.email_edit);
        addressEditText = (EditText) findViewById(R.id.address_editText);
        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        registerBtn = (Button) findViewById(R.id.next_register);
        nameTxt = (TextView) findViewById(R.id.name_txt);
        emailTxt = (TextView) findViewById(R.id.email_txt);
        phoneTxt = (TextView) findViewById(R.id.mobile_number_textView);



        genderTxt = (TextView) findViewById(R.id.gender_txt);
        birthTxt = (TextView) findViewById(R.id.birth_txt);
        welcomTxt = (TextView) findViewById(R.id.welcome_txt);
        addressTextView = (TextView) findViewById(R.id.address_textView);
    }


}
