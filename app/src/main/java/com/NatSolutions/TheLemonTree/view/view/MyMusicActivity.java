package com.NatSolutions.TheLemonTree.view.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.ConnectionManager;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.FavoriteTrackResponse;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.utils.Utils;
import com.NatSolutions.TheLemonTree.view.adapters.MyMusicAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hagar on 5/27/2017.
 */

public class MyMusicActivity extends AppCompatActivity {


    private TextView noDataTextView, titleTextView;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    MyMusicAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_music);
        noDataTextView = (TextView) findViewById(R.id.no_data_textView);
        titleTextView = (TextView) findViewById(R.id.title_textView);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        FontsUtils.setTypeFace(noDataTextView, Constants.LEMON_TREE_REGULAR);
        FontsUtils.setTypeFace(titleTextView, Constants.LEMON_TREE_REGULAR);
        initializeRecyclerView();
        getUserFavoriteTracks();
    }

    private void initializeRecyclerView() {
        mAdapter = new MyMusicAdapter(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    public void getUserFavoriteTracks() {
        progressBar.setVisibility(View.VISIBLE);
        Call<FavoriteTrackResponse> call = ConnectionManager.getApiManager().getUserFavouriteMusic(
                Integer.parseInt(SharedPreferencesManager.getInstance(this).getString(getResources()
                        .getString(R.string.user_logged_acess_pref))));
        call.enqueue(new Callback<FavoriteTrackResponse>() {
            @Override
            public void onResponse(Call<FavoriteTrackResponse> call, Response<FavoriteTrackResponse> response) {
                if (response.isSuccessful() && response.body().favoriteTracks != null) {
                    progressBar.setVisibility(View.GONE);
                    if (response.body().favoriteTracks.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        noDataTextView.setVisibility(View.GONE);
                        mAdapter.setData(response.body().favoriteTracks);
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        noDataTextView.setVisibility(View.VISIBLE);
                    }
                } else {
                    progressBar.setVisibility(View.GONE);
                    Utils.showPopUp("something went wrong!", MyMusicActivity.this);
                }
            }

            @Override
            public void onFailure(Call<FavoriteTrackResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Utils.showPopUp("something went wrong!", MyMusicActivity.this);
            }
        });
    }

}