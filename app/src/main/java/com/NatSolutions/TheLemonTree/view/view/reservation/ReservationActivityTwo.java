package com.NatSolutions.TheLemonTree.view.view.reservation;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.GuestList;
import com.NatSolutions.TheLemonTree.model.GuestListModel;
import com.NatSolutions.TheLemonTree.model.ReservationDataManager;
import com.NatSolutions.TheLemonTree.model.ReservationTypeItem;
import com.NatSolutions.TheLemonTree.model.ReservationTypeTimeSoltsItem;
import com.NatSolutions.TheLemonTree.view.componentes.dialogs.DateTime;
import com.NatSolutions.TheLemonTree.view.componentes.dialogs.DateTimePicker;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;
import com.NatSolutions.TheLemonTree.view.view.MyReservationsActivity;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Hagar on 5/11/2017.
 */

public class ReservationActivityTwo extends FragmentActivity implements OnRequestCompletedListener, DateTimePicker.OnDateTimeSetListener {
    ArrayList<ReservationTypeItem> reservations;
    String date, venueName, venueId;
    private TextView gusets_number, venue_name, reservation_date, time_txt, number_txt, numberOfpeopleTextView;
    private RelativeLayout date_picker_lay;
    private Button minus, plus, send_request_btn, reservationType1, reservationType2;
    LinearLayout layout_for, guests_layout;
    RelativeLayout add_guests_lay;
    Date defaultDate = new Date();
    OnRequestCompletedListener listenerr;
    ArrayList<GuestList> guests = new ArrayList<>();
    DateTime mDateTime = null;
    int lasttag = -1;
    private String selectedReservationTypeId, selectedReservationTimeSlotId;
    ArrayList<ReservationTypeTimeSoltsItem> slots = null;
    private int current = 0;
    private Typeface black, bold, regular;
    private TextView text1;
    private EditText edittext1;
    int count = 0;
    private String daterequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_two);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        reservations = Constants.getInstance().getReservationTypeItemList();
        Bundle extras = getIntent().getExtras();
        listenerr = this;
        if (extras != null) {
            venueName = extras.getString("venueName");
            venueId = extras.getString("venueId");
            date = extras.getString("date");
            daterequest = extras.getString("dater");

        }
        black = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Black.ttf");
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        venue_name = (TextView) findViewById(R.id.venue_name);
        text1 = (TextView) findViewById(R.id.text1);
        numberOfpeopleTextView = (TextView) findViewById(R.id.number_of_people_textView);
        text1.setTypeface(bold);
        edittext1 = (EditText) findViewById(R.id.edittt);
        edittext1.setTypeface(regular);
        venue_name.setTypeface(bold);
        venue_name.setText(venueName);
        reservation_date = (TextView) findViewById(R.id.reservation_date);
        reservation_date.setTypeface(regular);
        reservation_date.setText(date);
        time_txt = (TextView) findViewById(R.id.time_txt);
        number_txt = (TextView) findViewById(R.id.number_txt);
        gusets_number = (TextView) findViewById(R.id.gusets_number);
        date_picker_lay = (RelativeLayout) findViewById(R.id.date_picker_lay);
        send_request_btn = (Button) findViewById(R.id.send_request_btn);
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        reservationType1 = (Button) findViewById(R.id.reservation_type1);
        reservationType2 = (Button) findViewById(R.id.reservation_type2);
        add_guests_lay = (RelativeLayout) findViewById(R.id.add_guests_lay);
        layout_for = (LinearLayout) findViewById(R.id.layout_for);
        guests_layout = (LinearLayout) findViewById(R.id.guests_layout);
        add_guests_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (guests.size() < current) {
                    addGuestsDialog();
                } else {
                    Toast.makeText(ReservationActivityTwo.this, "You need to count up more Guests First", Toast.LENGTH_LONG).show();
                }
            }
        });
        date_picker_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                SimpleDateTimePicker.make(
//                        "Time", defaultDate,
//                        ReservationActivityTwo.this,
//                        getSupportFragmentManager()
//                ).showTime(defaultDate);
                if (slots != null) {
                    final Dialog dialog = new Dialog(ReservationActivityTwo.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custom_select_time_dialog);
                    ArrayList<String> arr = new ArrayList<String>();

                    for (int j = 0; j < slots.size(); j++) {
                        arr.add(slots.get(j).getTimeSlotValue());
                    }
                    final ListView items = (ListView) dialog.findViewById(R.id.time_list);
                    ArrayAdapter arrayAdapter = new ArrayAdapter(ReservationActivityTwo.this, R.layout.single_item, arr);
                    items.setAdapter(arrayAdapter);

                    items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            time_txt.setText(slots.get(i).getTimeSlotValue().toString());
                            selectedReservationTimeSlotId = slots.get(i).getTimeSlotId();
                            dialog.dismiss();
                        }
                    });


                    dialog.show();

                } else {
                    Toast.makeText(ReservationActivityTwo.this, "please select type of reservation first", Toast.LENGTH_LONG).show();
                }
            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current = Integer.parseInt(number_txt.getText().toString());
                current = current + 1;
                number_txt.setText("" + current);
                gusets_number.setText(+current + " guests");
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                current = Integer.parseInt(number_txt.getText().toString());
                if (current > 1) {
                    current = current - 1;
                    number_txt.setText("" + current);
                    gusets_number.setText(+current + " guests");

                }
            }
        });
        send_request_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GuestListModel guestListModell = new GuestListModel();
                ArrayList<GuestList> l = new ArrayList<GuestList>();
//                guestListModell.GuestList = guests;
                guestListModell.GuestList = l;


                openHouseRulesDialog();

            }
        });

        numberOfpeopleTextView.setTypeface(bold);
        send_request_btn.setTypeface(bold);
        addTypes(reservations);
    }

    private void openHouseRulesDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(ReservationActivityTwo.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.houses_rules_dialog);

        final TextView text = (TextView) dialog.findViewById(R.id.text);
        final TextView text1 = (TextView) dialog.findViewById(R.id.text1);
        final TextView text2 = (TextView) dialog.findViewById(R.id.text2);
        // final TextView text3 = (TextView) dialog.findViewById(R.id.text3);
        final TextView text4 = (TextView) dialog.findViewById(R.id.text4);
        final TextView text5 = (TextView) dialog.findViewById(R.id.text5);
        final TextView text6 = (TextView) dialog.findViewById(R.id.text6);
        final TextView text7 = (TextView) dialog.findViewById(R.id.text7);
        final TextView text8 = (TextView) dialog.findViewById(R.id.text8);
        final TextView text9 = (TextView) dialog.findViewById(R.id.text9);


        text.setTypeface(bold);
        text1.setTypeface(regular);
        text2.setTypeface(regular);
        //text3.setTypeface(regular);
        text4.setTypeface(regular);
        text5.setTypeface(regular);
        text6.setTypeface(regular);
        text7.setTypeface(regular);
        text8.setTypeface(regular);
        text9.setTypeface(regular);

        Button dialogButton = (Button) dialog.findViewById(R.id.Agree);
        Button dialogCancelButton = (Button) dialog.findViewById(R.id.Cancel);
        dialogButton.setTypeface(regular);
        dialogCancelButton.setTypeface(regular);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestList item = new GuestList();
                if (guests != null && venueId != null && selectedReservationTimeSlotId != null && selectedReservationTypeId != null && daterequest != null) {
                    LemonTreeApp.getInstance().showDialog(ReservationActivityTwo.this);

                    ReservationDataManager dataManager = new ReservationDataManager();
                    try {
                        dataManager.createNewReservation(ReservationActivityTwo.this, new Gson().toJson(guests), SharedPreferencesManager
                                        .getInstance(ReservationActivityTwo.this).getString(getResources().getString(R.string.user_logged_acess_pref))
                                , venueId, selectedReservationTypeId, selectedReservationTimeSlotId, daterequest
                                , "" + current, edittext1.getText().toString(), listenerr);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ReservationActivityTwo.this, "You Need To Complete Reservation Data First", Toast.LENGTH_LONG).show();
                }
                dialog.dismiss();
            }
        });
        dialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.show();

    }

    private void addGuestsDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.custom_add_guest_dialog);

        final EditText name = (EditText) dialog.findViewById(R.id.name_edit);
        final EditText phone = (EditText) dialog.findViewById(R.id.phone_edit);
        final TextView text1 = (TextView) dialog.findViewById(R.id.text1);
        final TextView text2 = (TextView) dialog.findViewById(R.id.text2);
        final TextView text3 = (TextView) dialog.findViewById(R.id.text3);

        name.setTypeface(regular);
        phone.setTypeface(regular);
        text1.setTypeface(bold);
        text2.setTypeface(bold);
        text3.setTypeface(bold);

        Button dialogButton = (Button) dialog.findViewById(R.id.add_guest_btn);
        dialogButton.setTypeface(regular);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestList item = new GuestList();
                item.GuestName = name.getText().toString();
                item.GuestPhoneNumber = phone.getText().toString();

                updateGuestsLayout(item, count);
                count++;
                guests.add(item);
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private void updateGuestsLayout(GuestList item, int index) {
        RelativeLayout view = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.guest_item_layout, null);

        TextView txt = (TextView) view.findViewById(R.id.textt);

        TextView remove = (TextView) view.findViewById(R.id.remove_item);
        txt.setText(item.GuestName);
        remove.setId(index);
        remove.setTag(index);
        txt.setTypeface(regular);
        ((LinearLayout) guests_layout).addView(view);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int Index = Integer.parseInt(view.getTag().toString());
                guests.remove(Index);
                ((ViewGroup) view.getParent()).removeAllViews();
            }
        });
    }


    public void updateBackgroundSelection(int i, int size) {
        for (int j = 0; j < size; j++) {
            if (lasttag != -1) {
                Button b = (Button) layout_for.findViewById(lasttag);
                b.setBackgroundResource(R.drawable.yellow_rectangle);
            }
        }
        Button b = (Button) layout_for.findViewById(i);
        b.setBackgroundResource(R.drawable.rectangle);

    }

    private void addTypes(final ArrayList<ReservationTypeItem> typesList) {
        reservationType1.setText(typesList.get(0).getReservationTypeName());
        reservationType2.setText(typesList.get(1).getReservationTypeName());
        reservationType1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationType1.setBackgroundResource(R.drawable.brown_small_btn);
                reservationType2.setBackgroundResource(R.drawable.gray_small_btn);
                selectedReservationTypeId = typesList.get(0).getReservationTypeId();
                slots = typesList.get(0).getReservationTypeTimeSolts();

            }
        });
        reservationType2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationType1.setBackgroundResource(R.drawable.gray_small_btn);
                reservationType2.setBackgroundResource(R.drawable.brown_small_btn);
                selectedReservationTypeId = typesList.get(1).getReservationTypeId();
                slots = typesList.get(1).getReservationTypeTimeSolts();
            }
        });


    }


    @Override
    public void onSuccess(String m, Object response) {

    }

    @Override
    public void onSuccess(String message, String response) {
        LemonTreeApp.getInstance().hideDialog();
        showPopUp(message);
    }

    public void showPopUp(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(ReservationActivityTwo.this, MyReservationsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .show();
    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {

    }

    @Override
    public void onFail(String ex) {
        LemonTreeApp.getInstance().hideDialog();

        new AlertDialog.Builder(this)
                .setMessage("Please choose another day for reservation")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();


    }

    @Override
    public void DateTimeSet(Date date) {
        mDateTime = new DateTime(date);
        String date_Formated = regularFormateDateAndTime(getResources().getString(R.string.fullDateFormat), mDateTime);
        time_txt.setText(date_Formated);
    }

    public static final String regularFormateDateAndTime(String formate, DateTime mDateTime) {
        SimpleDateFormat serverDateFormat = new SimpleDateFormat(formate);
        String time = serverDateFormat.format(mDateTime.getDate());
        return time;
    }
}
