package com.NatSolutions.TheLemonTree.view.view;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.AppDataManager;
import com.NatSolutions.TheLemonTree.model.BookingItem;
import com.NatSolutions.TheLemonTree.model.DataModel;
import com.NatSolutions.TheLemonTree.view.adapters.BookingsAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/27/2017.
 */

public class MyBookingFragment extends Fragment implements OnRequestCompletedListener {


    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<BookingItem> data;
    private NetworkUtil network;
    OnRequestCompletedListener listener;
    private TextView title;
    private Typeface regular;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.booking_fragment, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(getContext().getResources().getDrawable(R.drawable.line_dashed));
        recyclerView.addItemDecoration(dividerItemDecoration);
        network = new NetworkUtil();
        listener = this;
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (network.isInternetOn(getActivity())) {
            LemonTreeApp.getInstance().showDialog(getActivity());
            AppDataManager dataManager = new AppDataManager();
            try {
                dataManager.GetUserBookings(getActivity(), SharedPreferencesManager.getInstance(getActivity()).getString(getResources().getString(R.string.user_logged_acess_pref)), listener);
//                dataManager.GetUserBookings(getActivity(),"1020", listener);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");

        }
        return view;

    }


    @Override
    public void onSuccess(String message, Object response) {

    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {
        LemonTreeApp.getInstance().hideDialog();

        data = new ArrayList(response.length());
        ArrayList<DataModel> conceptList = new ArrayList<DataModel>();

        for (int i = 0; i < response.length(); i++) {
            try {
                data.add(new Gson().fromJson(response.getJSONObject(i).toString(), BookingItem.class));
                String description = "", name = "";

            } catch (Exception e) {
                Log.e("", "" + e);
            }
        }


        adapter = new BookingsAdapter(data,getActivity());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFail(String ex) {

    }


}

