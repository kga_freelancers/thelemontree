package com.NatSolutions.TheLemonTree.view.view.concepts;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.ConceptModel;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

/**
 * Created by Hagar on 5/26/2017.
 */

public class ConceptDetailsActivity extends AppCompatActivity {
    private SliderLayout sliderShow;
    private TextView name, date, attendeing_number, call_number, location, location_title;
    private Typeface regular;
    private ConceptModel concept;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_concept_details);
        concept = LemonTreeApp.getInstance().getSelectedConcept();
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        name = (TextView) findViewById(R.id.event_details_name);
        name.setTypeface(regular);
        name.setText(concept.ConceptName.toString());
        date = (TextView) findViewById(R.id.event_details_date);
        date.setTypeface(regular);
        date.setVisibility(View.GONE);
        date.setVisibility(View.GONE);
        attendeing_number = (TextView) findViewById(R.id.event_details_attending_number);
        attendeing_number.setTypeface(regular);
        attendeing_number.setVisibility(View.GONE);

        location = (TextView) findViewById(R.id.event_details_location);
        location.setTypeface(regular);

        location_title = (TextView) findViewById(R.id.location_title);
        location_title.setTypeface(regular);


        sliderShow = (SliderLayout) findViewById(R.id.slider);
        for (int i = 0; i < concept.ConceptImages.size(); i++) {
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView
                    .image(concept.ConceptImages.get(i).Image);

            sliderShow.addSlider(textSliderView);
        }
        sliderShow.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderShow.setCustomAnimation(new DescriptionAnimation());
        sliderShow.setDuration(2000);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.

        TextView expTv1 = (TextView) findViewById(R.id.expand_text_view);

// IMPORTANT - call setText on the ExpandableTextView to set the text content to display
        expTv1.setTypeface(regular);
        expTv1.setText(concept.ConceptDescription);
    }


    @Override
    protected void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    }

}