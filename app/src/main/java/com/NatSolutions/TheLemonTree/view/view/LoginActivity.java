package com.NatSolutions.TheLemonTree.view.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.LoginResponse;
import com.NatSolutions.TheLemonTree.model.RegisterFacebookUserModel;
import com.NatSolutions.TheLemonTree.model.UserAccountDataManager;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.bumptech.glide.Glide;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends Activity implements OnRequestCompletedListener {

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private Button log_fb_img;
    LoginButton loginButton;
    FacebookCallback<LoginResult> mCallBack;
    private RegisterFacebookUserModel user;
    private NetworkUtil network;
    private String userFacebookId;
    //    private TextView skip;
    private Typeface black, bold, regular;
    private TextView fourth, third, second, first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Glide.with(this).asGif().load(R.raw.splash_gif).into((ImageView) findViewById(R.id.imageView));

        String x = SharedPreferencesManager.getInstance(this).getString(getResources()
                .getString(R.string.user_logged_acess_pref));
        log_fb_img = (Button) findViewById(R.id.login_image_fb);

        if (!SharedPreferencesManager.getInstance(this).getString(getResources()
                .getString(R.string.user_name_pref), "").contentEquals("")) {
            log_fb_img.setVisibility(View.GONE);
        }

        black = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Black.ttf");
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        callbackManager = CallbackManager.Factory.create();
        network = new NetworkUtil();
        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallBack = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {

                                Log.e("response: ", response + "");
                                try {
                                    user = new RegisterFacebookUserModel();
                                    if (object.has("id"))
                                        user.setFacebookId(object.getString("id").toString());
                                    if (object.has("email"))
                                        user.setEmailAddress(object.getString("email").toString());
                                    if (object.has("name"))
                                        user.setName(object.getString("name").toString());
                                    if (object.has("gender"))
                                        user.setGender(object.getString("gender").toString());
                                    if (object.has("birthday"))
                                        user.setBirthDate(object.getString("birthday").toString());
                                    if (object.has("hometown"))
                                        user.setAddress(object.getString("hometown").toString());
                                    if (object.has("picture"))
                                        user.setImageUrl(object.getString("picture").toString());
                                    if (object.has("link"))
                                        user.setFacebookProfile(object.getString("link").toString());

                                    if (network.isInternetOn(LoginActivity.this)) {
                                        userFacebookId = user.getFacebookId();
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_birthdate_pref), user.getBirthDate());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_address_pref), user.getAddress());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_email_pref), user.getEmailAddress());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_name_pref), user.getName());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_gender_pref), user.getGender());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_phone_pref), user.getPhoneNumber());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_facebookid_pref), user.getFacebookId());
                                        SharedPreferencesManager.getInstance(LoginActivity.this).setString(getResources().getString(R.string.user_facebookprofile_pref), user.getFacebookProfile());


                                        Login(userFacebookId);
                                    } else {
                                        LemonTreeApp.getInstance().showToast("Network Error");

                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(LoginActivity.this, "Welcome " + user.getName(), Toast.LENGTH_LONG).show();
                            }

                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,picture, hometown, link, location");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        };

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));

    }

    private void Login(String FbId) {
        if (network.isInternetOn(LoginActivity.this)) {
            LemonTreeApp.getInstance().showDialog(LoginActivity.this);
            UserAccountDataManager dataManager = new UserAccountDataManager();
            try {
                dataManager.loginWithFacebook(LoginActivity.this, FbId, SharedPreferencesManager.getInstance(LoginActivity.this).getString("token"), this);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreferencesManager.getInstance(LoginActivity.this).getString(getResources().getString(R.string.user_facebookid_pref), null) != null &&
                        SharedPreferencesManager.getInstance(LoginActivity.this).getBoolean(getResources().getString(R.string.user_logged_pref), false) && SharedPreferencesManager.getInstance(LoginActivity.this).getString("token") != null) {
                    Login(SharedPreferencesManager.getInstance(LoginActivity.this).getString(getResources().getString(R.string.user_facebookid_pref)));

                } else if (SharedPreferencesManager.getInstance(LoginActivity.this).getString(getResources().getString(R.string.user_facebookid_pref), null) != null) {
                    UIManager.startSignUpActivity(LoginActivity.this);
                } else {

                    log_fb_img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                if (network.isInternetOn(LoginActivity.this)) {
//                    progrees.setVisibility(View.VISIBLE);
                            if (SharedPreferencesManager.getInstance(LoginActivity.this).getString("token") != null) {
                                loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));

                                loginButton.performClick();

                                loginButton.setPressed(true);

                                loginButton.invalidate();

                                loginButton.registerCallback(callbackManager, mCallBack);

                                loginButton.setPressed(false);

                                loginButton.invalidate();
                            } else {
                                Toast.makeText(getApplicationContext(), "Please wait a moment", Toast.LENGTH_LONG).show();
                            }

//                } else {
//                    Toast.makeText(LoginActivity.this, "No intenet Connection", Toast.LENGTH_LONG).show();
//
//                }

                        }
                    });

                }
            }
        }, 4000);



    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        super.onActivityResult(requestCode, responseCode, intent);
        //Facebook login
        if (requestCode == 3) {
        } else {
            callbackManager.onActivityResult(requestCode, responseCode, intent);
        }

    }

    //=================================================================//
    @Override
    public void onSuccess(String message, Object b) {
        String sResponse = b.toString();
        LemonTreeApp.getInstance().hideDialog();

//        ObjectMapper m = new ObjectMapper();
//        LoginResponse response = m.readValue(b.toString(), LoginResponse.class);
        Gson gson = new Gson();
        LoginResponse response = gson.fromJson(sResponse, LoginResponse.class);
        if (response.getMessage() != null && response.getMessage().toString().length() > 0) {
            Toast.makeText(this, response.getMessage().toString(), Toast.LENGTH_LONG).show();
        }
        if (response.getStatus().equalsIgnoreCase("true")) {

            if (response.getObj() != null && response.getObj().getClientId() != null) {
                SharedPreferencesManager.getInstance(this).setBoolean(getResources().getString(R.string.user_logged_pref), true);
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_logged_acess_pref), response.getObj().getClientId());
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_phone_pref), response.getObj().ClientPhoneNumber);
                if (response.getObj().ClientGender.equalsIgnoreCase("f")) {
                    SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_gender_pref), "female");
                } else {
                    SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_gender_pref), "male");

                }
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_name_pref), response.getObj().ClientName);
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_email_pref), response.getObj().ClientEmail);
                SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_address_pref), response.getObj().ClientAddress);
                if (response.getObj().ClientBrithdate.contains("T")) {
                    int i = response.getObj().ClientBrithdate.indexOf("T");
                    SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_birthdate_pref), response.getObj().ClientBrithdate.substring(0, i));

                } else {
                    SharedPreferencesManager.getInstance(this).setString(getResources().getString(R.string.user_birthdate_pref), response.getObj().ClientBrithdate);

                }
                UIManager.startMainActivity(LoginActivity.this);

            }


        } else {
            UIManager.startSignUpActivity(LoginActivity.this);

        }
    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {

    }


    @Override
    public void onFail(String ex) {
        LemonTreeApp.getInstance().hideDialog();
        Toast.makeText(this, ex, Toast.LENGTH_LONG).show();

    }


}
