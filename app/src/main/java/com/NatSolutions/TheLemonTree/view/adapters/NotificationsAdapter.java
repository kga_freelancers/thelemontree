package com.NatSolutions.TheLemonTree.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.NotificationModel;
import com.NatSolutions.TheLemonTree.model.SideMenuModel;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;

import java.util.ArrayList;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_BOLD;
import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_REGULAR;

/**
 * Created by amr on 26/08/17.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationViewHolder> {


    private List<NotificationModel> items = new ArrayList<>();

    private Context mContext;
    private LayoutInflater inflater;

    public NotificationsAdapter(Context context) {
        this.mContext = context;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public NotificationsAdapter.NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.index_notification, parent, false);
        return new NotificationsAdapter.NotificationViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final NotificationsAdapter.NotificationViewHolder holder, final int position) {
        holder.message.setText(items.get(position).message);
        holder.date.setText(items.get(position).date.substring(0, 10));
        // holder.status.setText(items.get(position).status);

        FontsUtils.setTypeFace(holder.message, LEMON_TREE_BOLD);
        FontsUtils.setTypeFace(holder.date, LEMON_TREE_REGULAR);
        FontsUtils.setTypeFace(holder.dateTitleTextView, LEMON_TREE_BOLD);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<NotificationModel> list) {
        this.items = list;
        notifyDataSetChanged();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {
        private TextView message;
        private TextView date;
        private TextView status;
        private TextView dateTitleTextView;

        public NotificationViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.message_textView);
            date = (TextView) itemView.findViewById(R.id.date_textView);
            dateTitleTextView = (TextView) itemView.findViewById(R.id.date_title_textView);
            //status = (TextView) itemView.findViewById(R.id.status_text);
            //thumImageView = (ImageView) itemView.findViewById(R.id.thum_imageView);
        }
    }
}




