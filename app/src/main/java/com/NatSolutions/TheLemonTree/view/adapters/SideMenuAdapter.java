package com.NatSolutions.TheLemonTree.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.SideMenuModel;
import com.NatSolutions.TheLemonTree.view.NotificationActivity;
import com.NatSolutions.TheLemonTree.view.view.MyMusicActivity;
import com.NatSolutions.TheLemonTree.view.view.MyReservationsActivity;

import java.util.ArrayList;
import java.util.List;

import static com.NatSolutions.TheLemonTree.model.Constants.ITEM_MUSIC_SIDE_MENU;
import static com.NatSolutions.TheLemonTree.model.Constants.ITEM_NOTIFICATION_SIDE_MENU;
import static com.NatSolutions.TheLemonTree.model.Constants.ITEM_RESERVATIONS_SIDE_MENU;

/**
 * Created by amr on 26/08/17.
 */

public class SideMenuAdapter extends RecyclerView.Adapter<SideMenuAdapter.NavigationViewHolder> {


    private List<SideMenuModel> items = new ArrayList<>();

    private Context mContext;
    private LayoutInflater inflater;
    private Typeface regular;

    public SideMenuAdapter(Context context) {
        this.mContext = context;
        inflater = LayoutInflater.from(context);

        items.add(new SideMenuModel("my notifications", R.drawable.ic_notifications));
        items.add(new SideMenuModel("my reservations", R.drawable.ic_reservations));
        items.add(new SideMenuModel("my music", R.drawable.ic_music));
        //   items.add(new SideMenuModel("My Profle",R.drawable.ic_menu_profile));
        regular = Typeface.createFromAsset(context.getAssets(), "AlegreyaSansSC-Regular.ttf");

    }

    @Override
    public SideMenuAdapter.NavigationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.index_navigation, parent, false);
        return new SideMenuAdapter.NavigationViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final SideMenuAdapter.NavigationViewHolder holder, final int position) {
        holder.nameTextView.setText(items.get(position).name);
        holder.sidemenuImageView.setImageResource(items.get(position).image);
        if (position == 0) holder.view.setVisibility(View.VISIBLE);
        else holder.view.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {
                    case ITEM_NOTIFICATION_SIDE_MENU:
                        mContext.startActivity(new Intent(mContext, NotificationActivity.class));
                        break;
                    case ITEM_RESERVATIONS_SIDE_MENU:
                        mContext.startActivity(new Intent(mContext, MyReservationsActivity.class));
                        break;
                    case ITEM_MUSIC_SIDE_MENU:
                        mContext.startActivity(new Intent(mContext, MyMusicActivity.class));
                        break;
                }

            }
        });
        holder.nameTextView.setTypeface(regular);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class NavigationViewHolder extends RecyclerView.ViewHolder {
        private TextView nameTextView;
        private ImageView sidemenuImageView;
        private View view;

        public NavigationViewHolder(View itemView) {
            super(itemView);
            nameTextView = (TextView) itemView.findViewById(R.id.index_textView);
            sidemenuImageView = (ImageView) itemView.findViewById(R.id.image);
            view = itemView.findViewById(R.id.first_line);
        }
    }
}




