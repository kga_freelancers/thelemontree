package com.NatSolutions.TheLemonTree.view.view.reservation;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.AppDataManager;
import com.NatSolutions.TheLemonTree.model.ReservationDetailsItem;
import com.NatSolutions.TheLemonTree.model.ReservationDetailsMainItem;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Hagar on 6/18/2017.
 */

public class BarCodeActivity extends FragmentActivity implements OnRequestCompletedListener {
    OnRequestCompletedListener arrayListener;
    private NetworkUtil network;
    private ImageView mail_share, whats_share, fb_share,bar_image,close;
    private TextView bar_code_screen_title, event_date, event_location, event_type, event_number_title, event_number_value, house_rules_txt, event_name;
    String type, id, name , paymentURL="";
    private Typeface black, bold, regular;
    private OnRequestCompletedListener listener;
    private LinearLayout pending_dialog, confirmd_dialog , deposit_layout;
    private Button cancel;
    ReservationDetailsMainItem  data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_bar_code);
        network = new NetworkUtil();
        listener = this;

        // TODO: 3/24/18 Uncomment code when deposit notification is done
        black = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Black.ttf");
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            type = extras.getString("type");
            id = extras.getString("id");
            name = extras.getString("name");
//            paymentURL = extras.getString("payment_url");
        }
        bar_code_screen_title = (TextView) findViewById(R.id.bar_code_screen_title);
        pending_dialog = (LinearLayout) findViewById(R.id.pending_dialog);
        confirmd_dialog = (LinearLayout) findViewById(R.id.confirmd_dialog);
        deposit_layout = (LinearLayout) findViewById(R.id.deposit_layout);
        cancel = (Button) findViewById(R.id.Cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelReservation(id);
            }
        });
        if (type.equalsIgnoreCase("pending")) {
            bar_code_screen_title.setText("reservation pending");
            pending_dialog.setVisibility(View.VISIBLE);
            confirmd_dialog.setVisibility(View.GONE);
        } else if (type.equalsIgnoreCase("confirmed")) {
            bar_code_screen_title.setText("reservation confirmed");
            pending_dialog.setVisibility(View.GONE);
            confirmd_dialog.setVisibility(View.VISIBLE);
        }
//        else if(type.equalsIgnoreCase("Deposit")){
//            bar_code_screen_title.setText("waiting deposit");
//            pending_dialog.setVisibility(View.GONE);
//            confirmd_dialog.setVisibility(View.GONE);
//            deposit_layout.setVisibility(View.VISIBLE);
//        }
        bar_code_screen_title.setTypeface(bold);

//        findViewById(R.id.pay_now_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(paymentURL));
//                startActivity(browserIntent);
//            }
//        });
//
//        findViewById(R.id.check_payment_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // TODO: 3/24/18 Call Check Payment Status API
//            }
//        });

        close = (ImageView) findViewById(R.id.close_activity);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIManager.startMyReservationActivity(BarCodeActivity.this);
                finish();
            }
        });
        event_date = (TextView) findViewById(R.id.event_date);
        event_date.setTypeface(regular);

        event_name = (TextView) findViewById(R.id.event_name);
        event_name.setTypeface(regular);

        event_location = (TextView) findViewById(R.id.event_location);
        event_location.setTypeface(regular);

        event_type = (TextView) findViewById(R.id.event_type);
        event_type.setTypeface(regular);

        event_number_title = (TextView) findViewById(R.id.event_number_title);
        event_number_title.setTypeface(regular);

        event_number_value = (TextView) findViewById(R.id.event_number_value);
        event_number_value.setTypeface(regular);

        house_rules_txt = (TextView) findViewById(R.id.house_rules_txt);
        house_rules_txt.setTypeface(regular);
        house_rules_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openHouseRulesDialog();            }
        });
        fb_share = (ImageView) findViewById(R.id.fb_share);
        fb_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.facebook.orca");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, "http://reservationapp.natlab.net"+data.Obj.ReservationInfo.get(0).BarcodeImagePath);
                startActivity(whatsappIntent);
            }
        });
        whats_share = (ImageView) findViewById(R.id.whats_share);
        whats_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data != null && data.Obj.ReservationInfo.get(0).BarcodeImagePath != null) {
                    Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                    whatsappIntent.setType("text/plain");
                    whatsappIntent.setPackage("com.whatsapp");
                    whatsappIntent.putExtra(Intent.EXTRA_TEXT, "http://reservationapp.natlab.net"+data.Obj.ReservationInfo.get(0).BarcodeImagePath);
                    startActivity(whatsappIntent);
                }
            }
        });
        mail_share = (ImageView) findViewById(R.id.mail_share);
        mail_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_SUBJECT, "The LemonTree App");
                intent.putExtra(Intent.EXTRA_TEXT,"http://reservationapp.natlab.net"+data.Obj.ReservationInfo.get(0).BarcodeImagePath );

                startActivity(Intent.createChooser(intent, "Send Email"));
            }
        });
        bar_image = (ImageView) findViewById(R.id.barcode_image);

        getReservationDetails(id);
    }

    private void cancelReservation(String id) {
        if (network.isInternetOn(BarCodeActivity.this)) {
            LemonTreeApp.getInstance().showDialog(BarCodeActivity.this);
            AppDataManager dataManager = new AppDataManager();
            try {
                dataManager.CnacelReservation(BarCodeActivity.this, id, listener);
//                dataManager.GetUserBookings(getActivity(),"1020", listener);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");

        }
    }

    private void getReservationDetails(String id) {
        if (network.isInternetOn(BarCodeActivity.this)) {
            LemonTreeApp.getInstance().showDialog(BarCodeActivity.this);
            AppDataManager dataManager = new AppDataManager();
            try {
                dataManager.GetReservationDetails(BarCodeActivity.this, id, listener);
//                dataManager.GetUserBookings(getActivity(),"1020", listener);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");

        }
    }

    @Override
    public void onSuccess(String message, Object response) {
        LemonTreeApp.getInstance().hideDialog();
        try {
            data=(new Gson().fromJson(response.toString(), ReservationDetailsMainItem.class));
            String description = "", name = "";

        } catch (Exception e) {
            Log.e("", "" + e);
        }
        updateFields(data.Obj.ReservationInfo.get(0));

    }

    @Override
    public void onSuccess(String message, String response) {
        LemonTreeApp.getInstance().hideDialog();

        try {
              data=(new Gson().fromJson(response, ReservationDetailsMainItem.class));
            String description = "", name = "";

        } catch (Exception e) {
            Log.e("", "" + e);
        }
        updateFields(data.Obj.ReservationInfo.get(0));
    }

    private void updateFields(ReservationDetailsItem data) {
        if(data.BarcodeImagePath!=null)
        {
            Picasso.with(BarCodeActivity.this)
                    .load("http://reservationapp.natlab.net"+data.BarcodeImagePath)
                    .placeholder(R.drawable.profile_pic_bg)
                    .error(R.drawable.profile_pic_bg)
                    .into(bar_image);
        }
        if(data.PeopleAlreadyAttend!=null)
        {
            event_number_value.setText(data.PeopleAlreadyAttend);
        }
        if(data.ReservationDate!=null)
        {
            int i=0;
            if(data.ReservationDate.contains("T")) {
                 i = data.ReservationDate.indexOf("T");
            }else {
                i=data.ReservationDate.length();
            }

            event_date.setText(data.ReservationDate.substring(0,i));
        }
        if(data.VenueName!=null)
        {
            event_location.setText(data.VenueName);
        }
//        if(data.!=null)
//        {
//            event_location.setText();
//
//        }
        if(data.ReservationTypeName!=null)
        {
            event_type.setText(data.ReservationTypeName);
        }

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {
//        LemonTreeApp.getInstance().hideDialog();
//        try {
//            data=(new Gson().fromJson(response.getJSONObject(0).toString(), ReservationDetailsMainItem.class));
//            String description = "", name = "";
//
//        } catch (Exception e) {
//            Log.e("", "" + e);
//        }
//        updateFields(data.Obj.ReservationInfo.get(0));
        UIManager.startMyReservationActivity(BarCodeActivity.this);
        finish();
    }

    @Override
    public void onFail(String ex) {
        LemonTreeApp.getInstance().hideDialog();

    }
    private void openHouseRulesDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(BarCodeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.houses_rules_dialog);

        final TextView text = (TextView) dialog.findViewById(R.id.text);
        final TextView text1 = (TextView) dialog.findViewById(R.id.text1);
        final TextView text2 = (TextView) dialog.findViewById(R.id.text2);
        //final TextView text3 = (TextView) dialog.findViewById(R.id.text3);
        final TextView text4 = (TextView) dialog.findViewById(R.id.text4);
        final TextView text5 = (TextView) dialog.findViewById(R.id.text5);
        final TextView text6 = (TextView) dialog.findViewById(R.id.text6);
        final TextView text7 = (TextView) dialog.findViewById(R.id.text7);
        final TextView text8 = (TextView) dialog.findViewById(R.id.text8);
        final TextView text9 = (TextView) dialog.findViewById(R.id.text9);


        text.setTypeface(bold);
        text1.setTypeface(regular);
        text2.setTypeface(regular);
        //text3.setTypeface(regular);
        text4.setTypeface(regular);
        text5.setTypeface(regular);
        text6.setTypeface(regular);
        text7.setTypeface(regular);
        text8.setTypeface(regular);
        text9.setTypeface(regular);

        Button dialogButton = (Button) dialog.findViewById(R.id.Agree);
        Button dialogCancelButton = (Button) dialog.findViewById(R.id.Cancel);
        dialogButton.setText("ok");
        dialogButton.setTypeface(regular);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialogCancelButton.setVisibility(View.GONE);

        dialog.show();

    }

    @Override
    public void onBackPressed() {
        UIManager.startMyReservationActivity(BarCodeActivity.this);
        finish();
    }
}
