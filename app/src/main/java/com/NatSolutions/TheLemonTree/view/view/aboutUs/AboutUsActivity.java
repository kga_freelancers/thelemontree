package com.NatSolutions.TheLemonTree.view.view.aboutUs;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;

import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_BOLD;
import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_REGULAR;

/**
 * Created by Hagar on 5/26/2017.
 */

public class AboutUsActivity extends Activity {


    private ImageView facebookImageView;
    private ImageView instagramImageView;
    private ImageView cloudImageView;
    private TextView theLemonTreeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        initViews();
        setClickListeners();
        setFonts();



    }

    public void setClickListeners() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        facebookImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri uri;
                try {
                    PackageManager pm = getPackageManager();
                    pm.getPackageInfo("com.facebook.katana", 0);
                    // http://stackoverflow.com/a/24547437/1048340
                    uri = Uri.parse("fb://facewebmodal/f?href=" + "https://www.facebook.com/TheLemonTreeCairo");
                } catch (PackageManager.NameNotFoundException e) {
                    uri = Uri.parse("https://www.facebook.com/TheLemonTreeCairo");
                }

                startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }
        });

        instagramImageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://instagram.com/_u//thelemontreeandco/");
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://instagram.com//thelemontreeandco/")));
                }
            }
        });


        cloudImageView.setOnClickListener(new View.OnClickListener()

                                          {

                                              @Override
                                              public void onClick(View v) {
                                                  Uri uri = Uri.parse("https://www.mixcloud.com/thelemontree/");
                                                  Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                                                  likeIng.setPackage("com.mixcloud.player");

                                                  try {
                                                      startActivity(likeIng);
                                                  } catch (ActivityNotFoundException e) {
                                                      startActivity(new Intent(Intent.ACTION_VIEW,
                                                              Uri.parse("https://www.mixcloud.com/thelemontree/")));
                                                  }
                                              }
                                          }

        );

        theLemonTreeTextView.setOnClickListener(new View.OnClickListener()

                                                {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.thelemontree.co/"));
                                                        startActivity(browserIntent);
                                                    }
                                                }

        );
    }


    private void setFonts() {
        FontsUtils.setTypeFaceForSubViews((RelativeLayout) findViewById(R.id.main_view), LEMON_TREE_REGULAR);
        FontsUtils.setTypeFace(findViewById(R.id.title_textView), LEMON_TREE_BOLD);
        FontsUtils.setTypeFace(findViewById(R.id.about_title_textView), LEMON_TREE_BOLD);
//        Utils.justify((TextView) findViewById(R.id.about_textView));
    }


    private void initViews() {
        facebookImageView = (ImageView) findViewById(R.id.facebook_imageview);
        instagramImageView = (ImageView) findViewById(R.id.instagram_imageView);
        cloudImageView = (ImageView) findViewById(R.id.cloud_imageview);
        theLemonTreeTextView = (TextView) findViewById(R.id.the_lemon_tree_textView);
    }

}