package com.NatSolutions.TheLemonTree.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.ConnectionManager;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.NotificationModel;
import com.NatSolutions.TheLemonTree.model.NotificationsResponse;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.view.adapters.NotificationsAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.NatSolutions.TheLemonTree.model.Constants.NOTIFICATION_RECEIVED;

public class NotificationActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private LinearLayoutManager manager;
    private NotificationsAdapter adapter;
    private TextView titleTextView;
    private TextView noDataTextView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        titleTextView = (TextView) findViewById(R.id.title_textView);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        noDataTextView = (TextView) findViewById(R.id.no_data_textView);
        DataUtil.saveData(LemonTreeApp.context, NOTIFICATION_RECEIVED, false);
        initializeRecyclerView();
        loadUserNotifications();
        FontsUtils.setTypeFace(titleTextView, Constants.LEMON_TREE_REGULAR);
        FontsUtils.setTypeFace(noDataTextView, Constants.LEMON_TREE_REGULAR);

    }

    private void initializeRecyclerView() {
        manager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);
        adapter = new NotificationsAdapter(this);
        mRecyclerView.setAdapter(adapter);
    }

    private void setData(List<NotificationModel> notificationModels) {
        adapter.setData(notificationModels);
        adapter.notifyDataSetChanged();
    }


    private void loadUserNotifications() {
        progressBar.setVisibility(View.VISIBLE);
        Call<NotificationsResponse> call = ConnectionManager.getApiManager()
                .getUserNotifications(Integer.parseInt(SharedPreferencesManager.getInstance(this).getString(getResources()
                        .getString(R.string.user_logged_acess_pref))));
        call.enqueue(new Callback<NotificationsResponse>() {
            @Override
            public void onResponse(Call<NotificationsResponse> call, Response<NotificationsResponse> response) {
                if (response.body() != null) {
                    setData(response.body().notificationModelList);
                    progressBar.setVisibility(View.GONE);
                    if (response.body().notificationModelList.size() == 0) {
                        noDataTextView.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationsResponse> call, Throwable t) {
                Toast.makeText(NotificationActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        });
    }
}
