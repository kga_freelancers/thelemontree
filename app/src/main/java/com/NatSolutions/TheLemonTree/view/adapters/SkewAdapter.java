package com.NatSolutions.TheLemonTree.view.adapters;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.DataModel;

import java.util.ArrayList;


public class SkewAdapter extends RecyclerView.Adapter<SkewAdapter.MyViewHolder> {

    private ArrayList<DataModel> dataSet;
    private Typeface regular;
    View.OnClickListener myOnClickListener;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewVersion;
        RelativeLayout background;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            this.textViewVersion = (TextView) itemView.findViewById(R.id.textViewVersion);
            this.background = (RelativeLayout) itemView.findViewById(R.id.card_bg);
        }
    }

    public SkewAdapter(ArrayList<DataModel> data, View.OnClickListener myOnClickListener) {
        this.dataSet = data;
        this.myOnClickListener = myOnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);
        regular = Typeface.createFromAsset(parent.getContext().getAssets(), "AlegreyaSansSC-Regular.ttf");

        view.setOnClickListener(myOnClickListener);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.textViewName.setText(dataSet.get(listPosition).getName());
//        holder.textViewVersion.setText(dataSet.get(listPosition).getVersion());
        holder.textViewName.setTypeface(regular);
//        holder.textViewVersion.setTypeface(regular);
        holder.background.setBackgroundResource(dataSet.get(listPosition).getImg());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
