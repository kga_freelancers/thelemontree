package com.NatSolutions.TheLemonTree.view.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.utils.MessageEvent;
import com.NatSolutions.TheLemonTree.view.adapters.HomeAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_NOTIFICATION;
import static com.NatSolutions.TheLemonTree.model.Constants.NOTIFICATION_RECEIVED;

/**
 * Created by amr on 26/08/17.
 */

public class HomeFragment extends Fragment {

    View view;

    private RelativeLayout mainView;
    private RecyclerView mRecyclerView;
    private HomeAdapter homeAdapter;
    private boolean openSideMenu = false;
    public sideMenuListener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);

        mainView = (RelativeLayout) view.findViewById(R.id.main_view);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        initializeRecyclerView();
        applyFontStyle();
        mListener = (HomeActivityNew) getActivity();
        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(MessageEvent event) {
        switch (event.message) {
            case NOTIFICATION_RECEIVED:
                homeAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void initializeRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        homeAdapter = new HomeAdapter(getActivity(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(homeAdapter);
    }

    public void openSideMenu() {
        if (mListener != null) {
            mListener.onSideMenuClicked(homeAdapter.openSideMenu());
        }

    }

    public static HomeFragment newInstance(String data) {
        Bundle args = new Bundle();
        args.putString(EXTRA_NOTIFICATION,data);
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }



    public void applyFontStyle() {
        FontsUtils.setTypeFaceForSubViews(mainView, Constants.LEMON_TREE_BOLD);
    }

    public interface sideMenuListener {
        void onSideMenuClicked(boolean isClicked);
    }
}
