package com.NatSolutions.TheLemonTree.view.view.restaurants;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.ResturantModel;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Hagar on 5/26/2017.
 */

public class ResturantDetailsActivity extends AppCompatActivity
        implements OnMapReadyCallback {
    private SliderLayout sliderShow;
    private Button make_reservatoin;
    private TextView name, date, attendeing_number, call_number, location, location_title;
    private Typeface regular;
    ImageView call, navigate;
    private ResturantModel restaurant;
    LinearLayout call_lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_event_details);
        restaurant = LemonTreeApp.getInstance().getSelectedRestaurant();
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        navigate = (ImageView) findViewById(R.id.navigate);
        name = (TextView) findViewById(R.id.event_details_name);
        name.setTypeface(regular);
        name.setText(restaurant.VenueName.toString());
        date = (TextView) findViewById(R.id.event_details_date);
        date.setTypeface(regular);
        date.setText(restaurant.NumberOfTables + " tables ");
        date.setVisibility(View.GONE);
        attendeing_number = (TextView) findViewById(R.id.event_details_attending_number);
        attendeing_number.setTypeface(regular);
        attendeing_number.setText(restaurant.MaxCapacity);
//        call_number = (TextView) findViewById(R.id.call_number);
////        call_number.setText("Mobile:" + event.);
//        call_number.setTypeface(regular);

        location = (TextView) findViewById(R.id.event_details_location);
        location.setTypeface(regular);

        location_title = (TextView) findViewById(R.id.location_title);
        location_title.setTypeface(regular);
        location.setText(restaurant.VenueAddress);

        make_reservatoin = (Button) findViewById(R.id.make_reservatoin);
        make_reservatoin.setTypeface(regular);
        make_reservatoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK,returnIntent);
                finish();            }
        });
        navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + restaurant.Latitude + "," + restaurant.Longitude + ""));
                startActivity(intent);
            }
        });
//        call_lay = (LinearLayout) findViewById(R.id.call_lay);
//        call_lay.setVisibility(View.VISIBLE);
//        call = (ImageView) findViewById(R.id.call);
//        call_number = (TextView) findViewById(R.id.call_number);
//        call_number.setTypeface(regular);

//        call.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + restaurant.VenueMobile + ""));
//                startActivity(intent);
//            }
//        });
        sliderShow = (SliderLayout) findViewById(R.id.slider);
        for (int i = 0; i < 3; i++) {
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView
                    .image(restaurant.RestaurantPhotoPath);

            sliderShow.addSlider(textSliderView);
        }
        sliderShow.setPresetTransformer(SliderLayout.Transformer.Accordion);
        sliderShow.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderShow.setCustomAnimation(new DescriptionAnimation());
        sliderShow.setDuration(2000);
        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        TextView expTv1 = (TextView) findViewById(R.id.expand_text_view);

// IMPORTANT - call setText on the ExpandableTextView to set the text content to display
        expTv1.setText(restaurant.RestaurantDescription);
    }


    @Override
    protected void onStop() {
        sliderShow.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            LatLng sydney = new LatLng(Double.parseDouble(restaurant.Latitude), Double.parseDouble(restaurant.Longitude));
            googleMap.addMarker(new MarkerOptions().position(sydney)
                    .title(restaurant.VenueName));
//            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15), 2000, null);

        } catch (Exception e) {

        }
    }
}

