package com.NatSolutions.TheLemonTree.view.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.adapters.SideMenuAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivityNew extends AppCompatActivity implements HomeFragment.sideMenuListener {

    private RelativeLayout mainView;
    private RecyclerView mRecyclerView;
    //    public DrawerLayout mDrawerLayout;
    DrawerLayout drawer;
    private SideMenuAdapter sideMenuAdapter;
    private LinearLayoutManager navigationLayoutManager;
    private TextView userNameTextView, userEmailTextView;
    private TextView poweredByTextView, natTextView;
    private String userName, userFacebookId, userEmail;
    private CircleImageView userImageView;
    private Typeface regular;
    private RelativeLayout headerLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_new_two);
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        mainView = (RelativeLayout) findViewById(R.id.main_view);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        headerLayout = (RelativeLayout) findViewById(R.id.header);
        userImageView = (CircleImageView) headerLayout.findViewById(R.id.user_imageView);
        userNameTextView = (TextView) headerLayout.findViewById(R.id.user_name_textView);
        userEmailTextView = (TextView) headerLayout.findViewById(R.id.user_email_textView);
        poweredByTextView = (TextView) findViewById(R.id.powered_by_textView);
        natTextView = (TextView) findViewById(R.id.nat_textView);
//        if(!DataUtil.getData(this,EXTRA_CANCEL_NOTIFICATION,"").contentEquals("")){
//            DataUtil.saveData(this,EXTRA_CANCEL_NOTIFICATION,"");
//            startActivity(new Intent(this,MyReservationsActivity.class));
//        }
//        else {
        openHomeFragment();
//        }
        initializeNavigationRecyclerView();
        setUserData();
        poweredByTextView.setTypeface(regular);
        natTextView.setTypeface(regular);

        natTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.nat-solutions.com/"));
                startActivity(browserIntent);
            }
        });
    }

    private void setUserData() {
        userNameTextView.setTypeface(regular);
        userEmailTextView.setTypeface(regular);
        userFacebookId = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookid_pref), null);
        userName = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_name_pref), null);
        userImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIManager.startEditProfile(HomeActivityNew.this);
            }
        });
        userEmail = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_phone_pref), null);
        if (userFacebookId != null) {
            String url = "https://graph.facebook.com/" + userFacebookId + "/picture?type=large";

            Picasso.with(HomeActivityNew.this)
                    .load(url)
                    .placeholder(R.drawable.profile_pic_bg)
                    .error(R.drawable.profile_pic_bg)
                    .into(userImageView);
        }
        userNameTextView.setText(userName);
        userEmailTextView.setText(userEmail);
    }

    private void initializeNavigationRecyclerView() {
        navigationLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(navigationLayoutManager);
        sideMenuAdapter = new SideMenuAdapter(this);
        mRecyclerView.setAdapter(sideMenuAdapter);
    }

    private void openHomeFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commit();
    }

    @Override
    public void onSideMenuClicked(boolean isClicked) {
        if (!drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.openDrawer(GravityCompat.START);
        } else {
            drawer.closeDrawer(GravityCompat.START);
        }
    }



   /* private void initializeRecyclerView(){
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new HomeAdapter(this));
    }
*/

  /*  public void applyFontStyle() {
        FontsUtils.setTypeFaceForSubViews(mainView, Constants.LEMON_TREE_BOLD);
//        FontsUtils.setTypeFace(conceptsSubtitleTextView, Constants.LEMON_TREE_REGULAR);
//        FontsUtils.setTypeFace(reservationSubTitleTextView, Constants.LEMON_TREE_REGULAR);
//        FontsUtils.setTypeFace(findViewById(R.id.music_subtitles_textView), Constants.LEMON_TREE_REGULAR);
//        FontsUtils.setTypeFace(findViewById(R.id.botique_subtitles_textView), Constants.LEMON_TREE_REGULAR);
//        FontsUtils.setTypeFace(findViewById(R.id.blog_subtitles_textView), Constants.LEMON_TREE_REGULAR);
//        FontsUtils.setTypeFace(findViewById(R.id.about_subtitles_textView), Constants.LEMON_TREE_REGULAR);
//        FontsUtils.setTypeFace(reservationSubTitleTextView, Constants.LEMON_TREE_REGULAR);
    }*/

   /* private void openSoonAlert() {
        // custom dialog
        final Dialog dialog = new Dialog(HomeActivityNew.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.alarm_txt_dialog);

        final TextView text1 = (TextView) dialog.findViewById(R.id.text1);


        text1.setTypeface(bold);

        Button dialogButton = (Button) dialog.findViewById(R.id.add_guest_btn);
        dialogButton.setTypeface(regular);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestList item = new GuestList();
                dialog.dismiss();
            }
        });

        dialog.show();

    }*/

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
            /*super.onBackPressed();*/
    }
}
