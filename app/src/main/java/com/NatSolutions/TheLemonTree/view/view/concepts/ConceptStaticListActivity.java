package com.NatSolutions.TheLemonTree.view.view.concepts;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.DataModel;
import com.NatSolutions.TheLemonTree.model.EventItem;
import com.NatSolutions.TheLemonTree.model.MyData;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.adapters.SkewAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;

import java.util.ArrayList;

/**
 * Created by Hagar on 5/26/2017.
 */

public class ConceptStaticListActivity extends AppCompatActivity {


    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    public static View.OnClickListener myOnClickListener;
    private NetworkUtil network;
    OnRequestCompletedListener listener;
    private TextView title;
    private Typeface regular;
    ArrayList<EventItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concepts);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        title = (TextView) findViewById(R.id.title);
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        title.setText("concepts");
        title.setTypeface(regular);
        recyclerView.setHasFixedSize(true);
        network = new NetworkUtil();
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        myOnClickListener = new ConceptStaticListActivity.MyOnClickListener(this);
        items = new ArrayList<>();
        for (int i = 0; i < MyData.conceptsTitleArray.length; i++) {
            EventItem item = new EventItem(MyData.conceptsTitleArray[i], MyData.conceptsDescripArray[i], MyData.conceptsDrawableArray[i], MyData.conceptsLatlang[i], MyData.conceptsAddress[i],MyData.conceptsLatitude[i],MyData.conceptsLongitude[i]);
            items.add(item);
        }

        ArrayList<DataModel> conceptList = new ArrayList<DataModel>();

        for (int i = 0; i < items.size(); i++) {


            String description = items.get(i).EventDescription;
            String name = items.get(i).EventName;
            DataModel d = new DataModel(name, description, MyData.conceptsDrawableArray[i]);
            conceptList.add(d);
        }
        adapter = new SkewAdapter(conceptList, myOnClickListener);
        recyclerView.setAdapter(adapter);
    }


    private  class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            int itemPosition = recyclerView.getChildLayoutPosition(v);
            LemonTreeApp.getInstance().setSelectedEvent(items.get(itemPosition));
            UIManager.startEventDetails((Activity) context);
        }
    }

}
