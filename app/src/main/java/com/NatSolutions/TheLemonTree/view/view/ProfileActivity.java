package com.NatSolutions.TheLemonTree.view.view;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.adapters.ProfilePagerAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_REGULAR;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ProfileActivity extends AppCompatActivity {

    private TextView profile_address, profile_name;
    private String userName, userFacebookId, userAddress;
    private CircleImageView profileImage;
    private ImageView profile_settings;
    private Typeface regular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("my reservations"));
        tabLayout.addTab(tabLayout.newTab().setText("my music"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        profile_name = (TextView) findViewById(R.id.profile_name);
        profile_address = (TextView) findViewById(R.id.profile_address);
        profile_name.setTypeface(regular);
        profile_address.setTypeface(regular);
        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        profile_settings = (ImageView) findViewById(R.id.profile_settings);
        userFacebookId = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookid_pref), null);
        userName = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_name_pref), null);
        profile_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIManager.startEditProfile(ProfileActivity.this);
            }
        });
        userAddress = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_address_pref), null);
        if (userFacebookId != null) {
            String url = "https://graph.facebook.com/" + userFacebookId + "/picture?type=large";

            Picasso.with(ProfileActivity.this)
                    .load(url)
                    .placeholder(R.drawable.profile_pic_bg)
                    .error(R.drawable.profile_pic_bg)
                    .into(profileImage);
        }
        profile_name.setText(userName);
        profile_address.setText(userAddress);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final ProfilePagerAdapter adapter = new ProfilePagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FontsUtils.setTypeFaceForSubViews((RelativeLayout)findViewById(R.id.main_layout),LEMON_TREE_REGULAR);
    }

}