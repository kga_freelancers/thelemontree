package com.NatSolutions.TheLemonTree.view.view.concepts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.EventItem;
import com.NatSolutions.TheLemonTree.model.MyData;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.adapters.ConceptsAdapter;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;

public class ConceptsListActivityNew extends AppCompatActivity {

    RelativeLayout mainView;
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concepts_list_new);

        mainView = (RelativeLayout) findViewById(R.id.main_view);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        initializeRecyclerView();
        FontsUtils.setTypeFaceForSubViews(mainView, Constants.LEMON_TREE_BOLD);

    }

    private void initializeRecyclerView() {
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new ConceptsAdapter(this));
    }


    public void startTargetActivity(int index) {
        EventItem eventItem = new EventItem(MyData.conceptsTitleArray[index], MyData.conceptsDescripArray[index]
                , MyData.conceptsDrawableArray[index], MyData.conceptsLatlang[index], MyData.conceptsAddress[index]
                , MyData.conceptsLatitude[index], MyData.conceptsLongitude[index]);

        LemonTreeApp.getInstance().setSelectedEvent(eventItem);
        UIManager.startEventDetails(ConceptsListActivityNew.this);

    }
}
