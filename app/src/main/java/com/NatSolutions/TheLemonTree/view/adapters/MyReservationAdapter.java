package com.NatSolutions.TheLemonTree.view.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.BookingItem;
import com.NatSolutions.TheLemonTree.view.UIManager;

import java.util.ArrayList;

/**
 * Created by amr on 26/08/17.
 */

public class MyReservationAdapter extends RecyclerView.Adapter<MyReservationAdapter.MyViewHolder> {

    private final Activity act;
    private ArrayList<BookingItem> dataSet;
    private Typeface regular, bold;
    View.OnClickListener myOnClickListener;

    public enum state {
        Confirmed, Notconfirmed, Pending, Cancelled,
        Waitingdeposit, ConfirmedPaid
    }

    state selected = null;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView eventName;
        TextView eventdate;
        TextView evenstate;
        TextView type;
        TextView eventNametitle;
        TextView eventdatetitle;
        TextView evenstatetitle;
        TextView typetitle;
        CardView reservationCardView;
        // LinearLayout item;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.eventName = (TextView) itemView.findViewById(R.id.name_textView);
            this.eventdate = (TextView) itemView.findViewById(R.id.date_textView);
            this.evenstate = (TextView) itemView.findViewById(R.id.status_textView);
            this.type = (TextView) itemView.findViewById(R.id.type_textView);
            this.eventNametitle = (TextView) itemView.findViewById(R.id.name_title_textView);
            this.eventdatetitle = (TextView) itemView.findViewById(R.id.date_title_textView);
            this.evenstatetitle = (TextView) itemView.findViewById(R.id.type_title_textView);
            this.typetitle = (TextView) itemView.findViewById(R.id.status_title_textView);
            this.reservationCardView = (CardView) itemView.findViewById(R.id.reservation_cardView);
            //   this.item = (LinearLayout) itemView.findViewById(R.id.booking_item_lay);
        }
    }

    public MyReservationAdapter(ArrayList<BookingItem> data, Activity act) {
//        ArrayList<BookingItem> items = new ArrayList<>();
//        for (int i = data.size() - 1; i >= 0; i--) {
//            items.add(data.get(i));
//        }
        this.dataSet = data;
        this.act = act;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.index_my_reservation, parent, false);
        regular = Typeface.createFromAsset(parent.getContext().getAssets(), "AlegreyaSansSC-Regular.ttf");
        bold = Typeface.createFromAsset(parent.getContext().getAssets(), "AlegreyaSansSC-Bold.ttf");

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.eventName.setText(dataSet.get(listPosition).VenueName);
        if (dataSet.get(listPosition).ReservationDate.toString().contains("T"))

        {
            int i = dataSet.get(listPosition).ReservationDate.toString().indexOf("T");
            holder.eventdate.setText(dataSet.get(listPosition).ReservationDate.toString().substring(0, i));
        } else {
            holder.eventdate.setText(dataSet.get(listPosition).ReservationDate.toString());

        }
        holder.type.setText(dataSet.get(listPosition).ReservationType);
        if (dataSet.get(listPosition).Cancelled == null && dataSet.get(listPosition).DepositAmount != null && dataSet.get(listPosition).DepositPayed != null && dataSet.get(listPosition).DepositPayed.equalsIgnoreCase("true")) {
            holder.evenstate.setText("confirmed & paid");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Green));
            holder.reservationCardView.setCardBackgroundColor(ContextCompat.getColor(act, android.R.color.white));
            selected = state.Confirmed;

        } else if (dataSet.get(listPosition).Confirmed != null && dataSet.get(listPosition).Confirmed.equals("false") && dataSet.get(listPosition).Cancelled == null) {
            holder.evenstate.setText("disconfirmed");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Red));
            holder.reservationCardView.setCardBackgroundColor(ContextCompat.getColor(act, R.color.black_10));
            selected = state.Notconfirmed;

        } else if (dataSet.get(listPosition).Cancelled == null && dataSet.get(listPosition).DepositAmount != null && dataSet.get(listPosition).DepositPayed != null && dataSet.get(listPosition).DepositPayed.equalsIgnoreCase("false")) {
            holder.evenstate.setText("waiting deposit");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Blue));
            holder.reservationCardView.setCardBackgroundColor(ContextCompat.getColor(act, R.color.black_10));
            selected = state.Waitingdeposit;


        } else if (dataSet.get(listPosition).Cancelled != null && dataSet.get(listPosition).Cancelled.equalsIgnoreCase("true")) {
            holder.evenstate.setText("cancelled");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.colorPrimary));
            holder.reservationCardView.setCardBackgroundColor(ContextCompat.getColor(act, R.color.black_10));
            selected = state.Cancelled;


        } else if (dataSet.get(listPosition).Cancelled == null && dataSet.get(listPosition).Confirmed == null) {
            holder.evenstate.setText("pending");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.colorPrimary));
            holder.reservationCardView.setCardBackgroundColor(ContextCompat.getColor(act, android.R.color.white));
            selected = state.Pending;


        } else if (dataSet.get(listPosition).Confirmed != null && dataSet.get(listPosition).Confirmed.equals("true") && dataSet.get(listPosition).Cancelled == null) {
            holder.evenstate.setText("confirmed");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Green));
            holder.reservationCardView.setCardBackgroundColor(ContextCompat.getColor(act, android.R.color.white));
            selected = state.Confirmed;
        }
        holder.eventName.setTypeface(regular);
        holder.eventdate.setTypeface(regular);
        holder.type.setTypeface(regular);
        holder.evenstate.setTypeface(regular);
        holder.eventNametitle.setTypeface(bold);
        holder.eventdatetitle.setTypeface(bold);
        holder.evenstatetitle.setTypeface(bold);
        holder.typetitle.setTypeface(bold);
        BookingItem bookingItem = dataSet.get(listPosition);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.evenstate.getText() != null) {
                    if (holder.evenstate.getText().toString().equals("confirmed")) {
                        UIManager.startBarCodeActivity(act, dataSet.get(listPosition).VenueName, "confirmed", dataSet.get(listPosition).ReservationId);
                    } else if (holder.evenstate.getText().toString().equals("disconfirmed")) {

                        //no action

                    } else if (holder.evenstate.getText().toString().equals("canclled")) {
                        //no action

                    } else if (holder.evenstate.getText().toString().equals("confirmed & paid")) {
                        UIManager.startBarCodeActivity(act, dataSet.get(listPosition).VenueName, "confirmed", dataSet.get(listPosition).ReservationId);

                    } else if (holder.evenstate.getText().toString().equals("pending")) {
                        UIManager.startBarCodeActivity(act, dataSet.get(listPosition).VenueName, "pending", dataSet.get(listPosition).ReservationId);


                    } else if (holder.evenstate.getText().toString().equals("waiting deposit")) {
//                        UIManager.startPaymentActivity(act);
                    }

                }
            }
        });


        //  sb.setSpan(new ForegroundColorSpan(Color.BLUE), 9, s.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}




