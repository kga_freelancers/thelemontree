package com.NatSolutions.TheLemonTree.view.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.ConnectionManager;
import com.NatSolutions.TheLemonTree.model.MyResponse;
import com.NatSolutions.TheLemonTree.model.Track;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.utils.Utils;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;
import com.NatSolutions.TheLemonTree.view.view.music.MusicActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_TRACK;
import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_TRACK_LIST;
import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_BOLD;
import static com.NatSolutions.TheLemonTree.model.Constants.LEMON_TREE_REGULAR;

/**
 * Created by amr on 26/08/17.
 */

public class MyMusicAdapter extends RecyclerView.Adapter<MyMusicAdapter.MyMusicViewHolder> {


    private List<Track> items = new ArrayList<>();

    private Context mContext;
    private LayoutInflater inflater;
    private String mTrackList;


    public MyMusicAdapter(Context context) {
        this.mContext = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyMusicAdapter.MyMusicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.index_my_music, parent, false);
        return new MyMusicAdapter.MyMusicViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final MyMusicAdapter.MyMusicViewHolder holder, final int position) {
        holder.progressBar.setVisibility(View.GONE);
        holder.removeFavorite.setVisibility(View.VISIBLE);

        final Track track = items.get(position);
        holder.title.setText(track.trackName);
        holder.musicName.setText(track.trackArtist);
        holder.time.setText(track.trckDescription);

        Picasso.with(mContext).load(track.trackIMage).into(holder.trackImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MusicActivity.class);
                intent.putExtra(EXTRA_TRACK, new Gson().toJson(track));
                intent.putExtra(EXTRA_TRACK_LIST, mTrackList);
                mContext.startActivity(intent);
            }
        });

        holder.removeFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.removeFavorite.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);
                Call<MyResponse> call = ConnectionManager.getApiManager().removeFavoriteTrack(track.trackId
                        , Integer.parseInt(SharedPreferencesManager.getInstance(mContext).getString(mContext.getResources()
                                .getString(R.string.user_logged_acess_pref))));
                call.enqueue(new Callback<MyResponse>() {
                    @Override
                    public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.removeFavorite.setVisibility(View.VISIBLE);
                        if (response.isSuccessful()) {
                            showPopUp("Track removed from favorite list",position);
                        } else {
                            Utils.showPopUp("Something went wrong!", mContext);
                        }
                    }

                    @Override
                    public void onFailure(Call<MyResponse> call, Throwable t) {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.removeFavorite.setVisibility(View.VISIBLE);
                        Utils.showPopUp("Something went wrong!", mContext);
                    }
                });
            }
        });

        FontsUtils.setTypeFace(holder.title, LEMON_TREE_BOLD);
        FontsUtils.setTypeFace(holder.musicName, LEMON_TREE_REGULAR);
        FontsUtils.setTypeFace(holder.time, LEMON_TREE_REGULAR);
    }


    public void showPopUp(String message , final int position) {
        try {
            new AlertDialog.Builder(mContext)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            items.remove(position);
                            notifyItemRemoved(position);
                            notifyDataSetChanged();
                        }
                    })
                    .show();
        } catch (Exception e) {
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<Track> items) {
        this.items = items;
        notifyDataSetChanged();
        mTrackList = new Gson().toJson(items, new TypeToken<List<Track>>() {
        }.getType());
    }

    public class MyMusicViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView musicName;
        private TextView time;
        private ImageView trackImage;
        private ImageView removeFavorite;
        private ProgressBar progressBar;

        public MyMusicViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_textView);
            musicName = (TextView) itemView.findViewById(R.id.music_name);
            time = (TextView) itemView.findViewById(R.id.time_textView);
            trackImage = (ImageView) itemView.findViewById(R.id.thum_imageView);
            removeFavorite = (ImageView) itemView.findViewById(R.id.remove_favorite_track);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }
}
