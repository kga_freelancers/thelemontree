package com.NatSolutions.TheLemonTree.view.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Track;
import com.NatSolutions.TheLemonTree.view.view.music.MusicActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_TRACK;
import static com.NatSolutions.TheLemonTree.model.Constants.EXTRA_TRACK_LIST;

/**
 * Created by amr on 10/11/17.
 */

public class MusicListAdapter extends RecyclerView.Adapter<MusicListAdapter.TrackViewHolder> {

    private Activity context;
    private List<Track> trackList;
    private LayoutInflater inflater;
    private String mTrackList;

    public MusicListAdapter(Activity context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        trackList = new ArrayList<>();
    }

    @Override
    public TrackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.index_music_list, parent, false);
        return new TrackViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrackViewHolder holder, int position) {
        final Track track = trackList.get(position);
        Picasso.with(context)
                .load(track.trackIMage)
                .into(holder.trackImageView);
        holder.trackNameTextView.setText(track.trackName);
        holder.artistNameTextView.setText(track.trackArtist);

        holder.playImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MusicActivity.class);
                intent.putExtra(EXTRA_TRACK, new Gson().toJson(track));
                intent.putExtra(EXTRA_TRACK_LIST, mTrackList);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return trackList.size();
    }


    public void setData(List<Track> trackList) {
        this.trackList = trackList;
        notifyDataSetChanged();
        mTrackList = new Gson().toJson(trackList, new TypeToken<List<Track>>() {
        }.getType());
    }

    public class TrackViewHolder extends RecyclerView.ViewHolder {
        private TextView trackNameTextView;
        private TextView artistNameTextView;
        private ImageView trackImageView;
        private ImageView playImageView;

        public TrackViewHolder(View itemView) {
            super(itemView);
            trackNameTextView = (TextView) itemView.findViewById(R.id.track_name_textView);
            artistNameTextView = (TextView) itemView.findViewById(R.id.artist_name_textView);
            trackImageView = (ImageView) itemView.findViewById(R.id.track_imageView);
            playImageView = (ImageView) itemView.findViewById(R.id.play_imageView);
        }
    }

}
