package com.NatSolutions.TheLemonTree.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.NatSolutions.TheLemonTree.view.view.MyBookingFragment;

/**
 * Created by Hagar on 5/27/2017.
 */

public class ProfilePagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ProfilePagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MyBookingFragment tab1 = new MyBookingFragment();
                return tab1;

            case 1:
               /* MyMusicFragment tab2 = new MyMusicFragment();
                return tab2;*/

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}