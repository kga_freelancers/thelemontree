package com.NatSolutions.TheLemonTree.view.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.ConceptModel;
import com.NatSolutions.TheLemonTree.model.EventItem;
import com.NatSolutions.TheLemonTree.model.ResturantModel;
import com.NatSolutions.TheLemonTree.view.componentes.SweetAlert.SweetProgressDialog;
import com.NatSolutions.TheLemonTree.view.componentes.SweetAlertDialog;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Hagar on 5/18/2017.
 */

public class LemonTreeApp extends MultiDexApplication {
    SweetProgressDialog dialog;

    private static LemonTreeApp mInstance = new LemonTreeApp();
    public static ConnectivityManager cm;

    public EventItem getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(EventItem selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public ResturantModel getSelectedRestaurant() {
        return selectedRestaurant;
    }

    public void setSelectedRestaurant(ResturantModel selectedRestaurant) {
        this.selectedRestaurant = selectedRestaurant;
    }

    @Override
    public void attachBaseContext(Context base) {
        MultiDex.install(base);
        super.attachBaseContext(base);
    }
    private ResturantModel selectedRestaurant;
    private EventItem selectedEvent;

    public ConceptModel getSelectedConcept() {
        return selectedConcept;
    }

    public void setSelectedConcept(ConceptModel selectedConcept) {
        this.selectedConcept = selectedConcept;
    }

    private ConceptModel selectedConcept;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
        context = getApplicationContext();
    }

    public static synchronized LemonTreeApp getInstance() {
        return mInstance;
    }

    public void showDialog(Context act) {
        if (dialog == null) {
            dialog = new SweetProgressDialog(act, SweetAlertDialog.PROGRESS_TYPE);
        }
        dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.colorAccent));
//        dialog.setTitleText(getResources().getString(R.string.loding_progress_message));
        dialog.setCancelable(false);
         dialog.show();
//        if (dialog == null) {
//            dialog = new TransparentProgressDialog(act, R.drawable.spinner);
//
//        }
//        dialog.setCancelable(true);
//        dialog.show();
    }

    public void hideDialog() {
        try {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void showToast(String s) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }
}
