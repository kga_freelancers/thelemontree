package com.NatSolutions.TheLemonTree.view.view.botique;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;

/**
 * Created by Hagar on 5/26/2017.
 */

public class BotiqueActivity extends Activity {
    private Typeface regular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soon);
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");

        TextView msg = (TextView) findViewById(R.id.msg);
        msg.setText("soon");
        msg.setTypeface(regular);

    }
}