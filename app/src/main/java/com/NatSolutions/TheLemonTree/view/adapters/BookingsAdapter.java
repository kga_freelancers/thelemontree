package com.NatSolutions.TheLemonTree.view.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.BookingItem;
import com.NatSolutions.TheLemonTree.view.UIManager;

import java.util.ArrayList;


public class BookingsAdapter extends RecyclerView.Adapter<BookingsAdapter.MyViewHolder> {

    private final Activity act;
    private ArrayList<BookingItem> dataSet;
    private Typeface regular;
    View.OnClickListener myOnClickListener;

    public enum state {
        Confirmed, Notconfirmed, Pending, Cancelled,
        Waitingdeposit, ConfirmedPaid
    }

    state selected = null;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView eventName;
        TextView eventdate;
        TextView evenstate;
        TextView type;
        TextView eventNametitle;
        TextView eventdatetitle;
        TextView evenstatetitle;
        TextView typetitle;
        LinearLayout item;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.eventName = (TextView) itemView.findViewById(R.id.vnue_name);
            this.eventdate = (TextView) itemView.findViewById(R.id.vnue_date);
            this.evenstate = (TextView) itemView.findViewById(R.id.vnue_type);
            this.type = (TextView) itemView.findViewById(R.id.vnue_state);
            this.eventNametitle = (TextView) itemView.findViewById(R.id.venue_name_title);
            this.eventdatetitle = (TextView) itemView.findViewById(R.id.venue_date_title);
            this.evenstatetitle = (TextView) itemView.findViewById(R.id.venue_type_title);
            this.typetitle = (TextView) itemView.findViewById(R.id.venue_state_title);
            this.item = (LinearLayout) itemView.findViewById(R.id.booking_item_lay);
        }
    }

    public BookingsAdapter(ArrayList<BookingItem> data, Activity act) {
        this.dataSet = data;
        this.act = act;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.booking_item, parent, false);
        regular = Typeface.createFromAsset(parent.getContext().getAssets(), "AlegreyaSansSC-Regular.ttf");

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        holder.eventName.setText(dataSet.get(listPosition).VenueName);
        if (dataSet.get(listPosition).ReservationDate.toString().contains("T"))

        {
            int i = dataSet.get(listPosition).ReservationDate.toString().indexOf("T");
            holder.eventdate.setText(dataSet.get(listPosition).ReservationDate.toString().substring(0, i));
        } else {
            holder.eventdate.setText(dataSet.get(listPosition).ReservationDate.toString());

        }
        holder.type.setText(dataSet.get(listPosition).ReservationType);
        if (dataSet.get(listPosition).Cancelled == null && dataSet.get(listPosition).DepositAmount != null && dataSet.get(listPosition).DepositPayed != null && dataSet.get(listPosition).DepositPayed.equalsIgnoreCase("true")) {
            holder.evenstate.setText("confirmed & paid");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Green));
            selected = state.Confirmed;

        }
       else if (dataSet.get(listPosition).Confirmed != null && dataSet.get(listPosition).Confirmed.equals("false") && dataSet.get(listPosition).Cancelled == null) {
            holder.evenstate.setText("disconfirmed");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Red));
            selected = state.Notconfirmed;

        }

        else
        if (dataSet.get(listPosition).Cancelled == null && dataSet.get(listPosition).DepositAmount != null && dataSet.get(listPosition).DepositPayed != null && dataSet.get(listPosition).DepositPayed.equalsIgnoreCase("false")) {
            holder.evenstate.setText("waiting deposit");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Blue));
            selected = state.Waitingdeposit;


        } else if (dataSet.get(listPosition).Cancelled != null && dataSet.get(listPosition).Cancelled.equalsIgnoreCase("true")) {
            holder.evenstate.setText("cancelled");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.colorPrimary));
            selected = state.Cancelled;


        } else  if (dataSet.get(listPosition).Cancelled == null && dataSet.get(listPosition).Confirmed == null) {
            holder.evenstate.setText("pending");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.colorPrimary));
            selected = state.Pending;


        } else
        if (dataSet.get(listPosition).Confirmed != null && dataSet.get(listPosition).Confirmed.equals("true") && dataSet.get(listPosition).Cancelled == null) {
            holder.evenstate.setText("confirmed");
            holder.evenstate.setTextColor(act.getResources().getColor(R.color.Green));
            selected = state.Confirmed;
        }
        holder.eventName.setTypeface(regular);
        holder.eventdate.setTypeface(regular);
        holder.type.setTypeface(regular);
        holder.evenstate.setTypeface(regular);
        holder.eventNametitle.setTypeface(regular);
        holder.eventdatetitle.setTypeface(regular);
        holder.evenstatetitle.setTypeface(regular);
        holder.typetitle.setTypeface(regular);
        BookingItem bookingItem = dataSet.get(listPosition);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.evenstate.getText() != null) {
                    if (holder.evenstate.getText().toString().equals("confirmed")) {
                        UIManager.startBarCodeActivity(act, dataSet.get(listPosition).VenueName, "confirmed", dataSet.get(listPosition).ReservationId);
                    } else if (holder.evenstate.getText().toString().equals("Disconfirmed")) {

                        //no action

                    } else if (holder.evenstate.getText().toString().equals("canclled")) {
                        //no action

                    } else if (holder.evenstate.getText().toString().equals("confirmed & paid")) {
                        UIManager.startBarCodeActivity(act, dataSet.get(listPosition).VenueName, "confirmed", dataSet.get(listPosition).ReservationId);

                    } else if (holder.evenstate.getText().toString().equals("pending")) {
                        UIManager.startBarCodeActivity(act, dataSet.get(listPosition).VenueName, "pending", dataSet.get(listPosition).ReservationId);


                    } else if (holder.evenstate.getText().toString().equals("waiting deposit")) {
//                        UIManager.startPaymentActivity(act);
                    }

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
