package com.NatSolutions.TheLemonTree.view.view;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.NatSolutions.TheLemonTree.R;
import com.bumptech.glide.Glide;


public class SplashActivity extends Activity {

    ImageView splashImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashImageView = findViewById(R.id.splash_imageView);

        Glide.with(this).asGif().load(R.raw.splash_gif).into(splashImageView);

//        Thread timer = new Thread() {
//            public void run() {
//                try {
//                    //Display for 3 seconds
//                    sleep(3000);
//                } catch (InterruptedException e) {
//                    // TODO: handle exception
//                    e.printStackTrace();
//                } finally {
//                    UIManager.startlogin(SplashActivity.this);
//                }
//            }
//        };
//        timer.start();
    }


}

