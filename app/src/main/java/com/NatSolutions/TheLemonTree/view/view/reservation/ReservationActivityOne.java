package com.NatSolutions.TheLemonTree.view.view.reservation;

import android.animation.LayoutTransition;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.base.OnRequestCompletedListener;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.ReservationDataManager;
import com.NatSolutions.TheLemonTree.model.ReservationTypeItem;
import com.NatSolutions.TheLemonTree.model.VenueItem;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.componentes.NetworkUtil;
import com.NatSolutions.TheLemonTree.view.componentes.dialogs.DateTime;
import com.NatSolutions.TheLemonTree.view.componentes.dialogs.DateTimePicker;
import com.NatSolutions.TheLemonTree.view.componentes.dialogs.SimpleDateTimePicker;
import com.NatSolutions.TheLemonTree.view.view.LemonTreeApp;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Hagar on 5/11/2017.
 */

public class ReservationActivityOne extends FragmentActivity implements OnRequestCompletedListener, DateTimePicker.OnDateTimeSetListener {

    private CircleImageView user_profile__img;
    private RelativeLayout reservation_lay;
    private Button location, when, next;
    ScrollView scroll;
    OnRequestCompletedListener arrayListener;
    private LinearLayout dropLayout;
    private ArrayList<VenueItem> whereList;
    private boolean finish = false;
    DateTime mDateTime = null;
    Date defaultDate = new Date();
    private RelativeLayout locationLay;
    private Button temp_location_btn, toButtonLay;
    String venueId = null, dateFormated = null, venueName = "";
    private String date_Formated;
    private String date_Formated_english;
    private Typeface regular, bold;
    private TextView text1, text2, text3, text4;
    private NetworkUtil network;
    private int locationLayClicked, toLayClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_one);
        network = new NetworkUtil();

        locationLayClicked = 0;
        toLayClicked = 0;
        location = (Button) findViewById(R.id.location);
        text1 = (TextView) findViewById(R.id.text_f);
        text2 = (TextView) findViewById(R.id.text_s);
        text3 = (TextView) findViewById(R.id.text_t);
        text4 = (TextView) findViewById(R.id.text_fo);
        location.setClickable(false);
        temp_location_btn = (Button) findViewById(R.id.temp_location_btn);
        locationLay = (RelativeLayout) findViewById(R.id.location_lay);
        dropLayout = (LinearLayout) findViewById(R.id.drop_down_lay);
        scroll = (ScrollView) findViewById(R.id.scroll);
        toButtonLay = (Button) findViewById(R.id.to_button_lay);
        arrayListener = this;
        when = (Button) findViewById(R.id.when);
        next = (Button) findViewById(R.id.next);

        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");

        next.setTypeface(regular);
        when.setTypeface(regular);
        location.setTypeface(regular);
        temp_location_btn.setTypeface(regular);
        text1.setTypeface(bold);
        text2.setTypeface(bold);
        text3.setTypeface(bold);
        text4.setTypeface(bold);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            venueName = bundle.getString("venue", null);
            venueId = bundle.getString("venueId", null);
        }
        if (venueName == null || venueId == null) {
            getLocations();
        } else {
            location.setText(venueName);

        }
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dateFormated != null & venueId != null) {
                    getReservationState(venueId, date_Formated_english);
                } else {
                    Toast.makeText(ReservationActivityOne.this, "Please Pick Missing Info", Toast.LENGTH_LONG).show();
                }
            }
        });
        temp_location_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideLocationDropDownAnimation(dropLayout, locationLay);

            }
        });
//        locationLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hideLocationDropDownAnimation(dropLayout, locationLay);
//
//            }
//        });
        when.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateTimePicker.make(
                        " ", defaultDate,
                        ReservationActivityOne.this,
                        getSupportFragmentManager()
                ).show(defaultDate);
            }
        });

        dropLayout.bringToFront();
        dropLayout.invalidate();
        ((View) dropLayout.getParent()).requestLayout();

        scroll.bringToFront();

        locationLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* locationLayClicked++;
                if (locationLayClicked % 2 == 0) {
                    hideLocationDropDownAnimation(dropLayout, locationLay);
                } else {
                    dropLayout = (LinearLayout) findViewById(R.id.drop_down_lay);
                    if (whereList.size() > 0) {
                        showLocationDropDownAnimation(dropLayout, locationLay);
                    }


                }*/

                if (dropLayout.getVisibility() == View.VISIBLE)
                    hideLocationDropDownAnimation(dropLayout, locationLay);
                else
                    showLocationDropDownAnimation(dropLayout, locationLay);


            }
        });

       /* toButtonLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toLayClicked++;
                if (toLayClicked % 2 == 0) {
                    hideLocationDropDownAnimation(dropLayout, locationLay);
                } else {
                    dropLayout = (LinearLayout) findViewById(R.id.drop_down_lay);
                    if (whereList.size() > 0) {
                        showLocationDropDownAnimation(dropLayout, locationLay);
                    }
                }
            }

        });*/

        initilizeCompopnents();
    }

    private void getReservationState(String venueId, String date) {
        if (network.isInternetOn(ReservationActivityOne.this)) {
            LemonTreeApp.getInstance().showDialog(ReservationActivityOne.this);
            ReservationDataManager dataManager = new ReservationDataManager();
            try {
                dataManager.CheckReservationBookingDateHasEvent(this, date, venueId, arrayListener);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            LemonTreeApp.getInstance().showToast("Network Error");

        }
    }

    private void getLocations() {
        ReservationDataManager dataManager = new ReservationDataManager();
        try {
            dataManager.GetAllVenues(this, null, arrayListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showLocationDropDownAnimation(LinearLayout layout, RelativeLayout locationLay) {
        location.setVisibility(View.INVISIBLE);
        locationLay.setVisibility(View.VISIBLE);
        toButtonLay.setVisibility(View.GONE);
        layout.setVisibility(View.VISIBLE);
        LayoutTransition lt = new LayoutTransition();
        lt.disableTransitionType(LayoutTransition.APPEARING);
        layout.setLayoutTransition(lt);
        addItems(0, layout, locationLay);

    }


    private void hideLocationDropDownAnimation(LinearLayout layout, RelativeLayout locationLay) {
        //  location.setVisibility(View.VISIBLE);
        // locationLay.setVisibility(View.INVISIBLE);
        location.setVisibility(View.INVISIBLE);
        locationLay.setVisibility(View.VISIBLE);
        toButtonLay.setVisibility(View.VISIBLE);
        layout.setVisibility(View.GONE);
        layout.removeAllViewsInLayout();
        layout.bringToFront();
        layout.invalidate();
        ((View) layout.getParent()).requestLayout();
        layout.removeAllViews();
        LayoutTransition lt = new LayoutTransition();
        lt.disableTransitionType(LayoutTransition.DISAPPEARING);
        layout.setLayoutTransition(lt);
        finish = false;

    }


    private void addItems(int i, final LinearLayout layout, final RelativeLayout locationLayout) {
        layout.bringToFront();
        layout.invalidate();
        ((View) layout.getParent()).requestLayout();
        RelativeLayout view = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.from_spinner_item_lay, null);

        Button button = (Button) view.findViewById(R.id.button);
        button.setTag(i);
        button.setText(whereList.get(i).getVenueName().toString());
        button.setTypeface(regular);

        if (i == 0) {
            button.setBackgroundResource(R.drawable.black_btn_trans_two);
        } else {
            button.setBackgroundResource(R.drawable.black_btn_trans);

        }

        ((LinearLayout) dropLayout).addView(view);
        dropLayout.bringToFront();
        dropLayout.invalidate();
        ((View) dropLayout.getParent()).requestLayout();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int tagValue = (Integer) view.getTag();
                location.setVisibility(View.VISIBLE);
                venueName = whereList.get(tagValue).getVenueName();

                // location.setText(venueName);
                //temp_location_btn.setText(venueName);
                toButtonLay.setText(venueName);
                venueId = whereList.get(tagValue).getVenueId();
                hideLocationDropDownAnimation(layout, locationLayout);


            }
        });
        Animation fadeIn = AnimationUtils.loadAnimation(ReservationActivityOne.this, R.anim.mainfadein);
        view.startAnimation(fadeIn);
        i++;
        final int j = i;

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (j < (whereList.size())) {
                    addItems(j, layout, locationLayout);
                } else {

                    finish = true;

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void initilizeCompopnents() {

    }

    @Override
    public void onSuccess(String m, Object response) {

    }

    @Override
    public void onSuccess(String message, String response) {

    }

    @Override
    public void onSuccessArray(JSONArray response, String message, String type) {
        if (message.length() > 0) {
          //  Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
        if (type.equalsIgnoreCase("Venues")) {
            if (response != null) {
                whereList = new ArrayList(response.length());
                for (int i = 0; i < response.length(); i++) {
                    try {
                        whereList.add(new Gson().fromJson(response.getJSONObject(i).toString(), VenueItem.class));
                    } catch (Exception e) {

                    }
                }
                if (whereList.size() > 0) {
                    location.setClickable(true);
                }
            }
        } else if (type.equalsIgnoreCase("reservationData")) {
            LemonTreeApp.getInstance().hideDialog();

            if (response != null) {
                ArrayList<ReservationTypeItem> reservations = new ArrayList(response.length());
                for (int i = 0; i < response.length(); i++) {
                    try {
                        reservations.add(new Gson().fromJson(response.getJSONObject(i).toString(), ReservationTypeItem.class));
                    } catch (Exception e) {

                    }
                }
                Constants.getInstance().setReservationTypeItemList(reservations);
                UIManager.startReservationSecondActivity(ReservationActivityOne.this, 2, venueName, venueId, date_Formated, date_Formated_english);

            }

        } else {
            getReservationData(venueId);
        }


    }

    private void getReservationData(String venueId) {
        ReservationDataManager dataManager = new ReservationDataManager();
        try {
            dataManager.CreatetReservationTypeAndTimeSlotsByVenueId(this, venueId, arrayListener);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFail(String ex) {
        location.setClickable(false);
        LemonTreeApp.getInstance().hideDialog();
        Toast.makeText(this, ex, Toast.LENGTH_LONG).show();

    }

    @Override
    public void DateTimeSet(Date date) {
        mDateTime = new DateTime(date);
        dateFormated = regularFormateDateAndTime(getResources().getString(R.string.dateFormat), mDateTime);

        date_Formated = regularFormateDateAndTime(getResources().getString(R.string.fullDateFormat), mDateTime);
        SimpleDateFormat sp = new SimpleDateFormat(getResources().getString(R.string.dateFormat), Locale.ENGLISH);
        date_Formated_english = sp.format(mDateTime.getDate());
        when.setText(date_Formated);
    }

    public static final String regularFormateDateAndTime(String formate, DateTime mDateTime) {
        SimpleDateFormat serverDateFormat = new SimpleDateFormat(formate);
        String time = serverDateFormat.format(mDateTime.getDate());
        return time;
    }
}
