package com.NatSolutions.TheLemonTree.view.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.DataModel;
import com.NatSolutions.TheLemonTree.model.GuestList;
import com.NatSolutions.TheLemonTree.model.MyData;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.adapters.SkewAdapter;
import com.NatSolutions.TheLemonTree.view.componentes.storage.SharedPreferencesManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Hagar on 5/6/2017.
 */

public class HomeActivity extends AppCompatActivity {


    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    public static View.OnClickListener myOnClickListener;
    private String userFacebookId;
    private CircleImageView profileImage;
    private ImageView fb, tw;
    private Typeface bold, regular;
    private TextView first, second, third;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        bold = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Bold.ttf");
        regular = Typeface.createFromAsset(getAssets(), "AlegreyaSansSC-Regular.ttf");
        first = (TextView) findViewById(R.id.title_f);
        second = (TextView) findViewById(R.id.title_s);
        third = (TextView) findViewById(R.id.title_t);
        first.setTypeface(bold);
        second.setTypeface(bold);
        third.setTypeface(bold);
//        fb = (ImageView) findViewById(R.id.fb);
//        tw = (ImageView) findViewById(R.id.tw);
//        fb.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//        tw.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // UIManager.startProfile(HomeActivity.this);
            }
        });
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        myOnClickListener = new MyOnClickListener(this);
        userFacebookId = SharedPreferencesManager.getInstance(this).getString(getResources().getString(R.string.user_facebookid_pref), null);
        if (userFacebookId != null) {
            String url = "https://graph.facebook.com/" + userFacebookId + "/picture?type=large";

            Picasso.with(HomeActivity.this)
                    .load(url)
                    .placeholder(R.drawable.profile_pic_bg)
                    .error(R.drawable.profile_pic_bg)
                    .into(profileImage);
        }
        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.descriptionArray[i], MyData.drawableArray[i]
            ));
        }


        adapter = new SkewAdapter(data, myOnClickListener);
        recyclerView.setAdapter(adapter);
    }

    private class MyOnClickListener implements View.OnClickListener {

        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            int itemPosition = recyclerView.getChildLayoutPosition(v);
            switch (itemPosition) {
                case 0:
                    UIManager.startConceptsActivity((Activity) context);
                    break;
                case 1:
                    UIManager.startReservationActivity((Activity) context, null, null);
                    break;
                case 2:
//                    UIManager.startMusic((Activity) context);

                    break;
                case 3:
//                    UIManager.startBlog((Activity) context);
                    openSoonAlert();
                    break;
                case 4:
//                    UIManager.startBotique((Activity) context);
                    openSoonAlert();

                    break;
                case 5:
                   UIManager.startAboutUs((Activity) context);
                  //  openSoonAlert();

                    break;

            }
        }
    }

    private void openSoonAlert() {
        // custom dialog
        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.alarm_txt_dialog);

        final TextView text1 = (TextView) dialog.findViewById(R.id.text1);


        text1.setTypeface(bold);

        Button dialogButton = (Button) dialog.findViewById(R.id.add_guest_btn);
        dialogButton.setTypeface(regular);

        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestList item = new GuestList();
                dialog.dismiss();
            }
        });

        dialog.show();

    }

}



