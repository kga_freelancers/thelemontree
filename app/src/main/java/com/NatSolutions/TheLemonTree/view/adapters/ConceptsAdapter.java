package com.NatSolutions.TheLemonTree.view.adapters;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.HomeItemsModel;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.view.view.concepts.ConceptsListActivityNew;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karim on 22/07/17.
 */

public class ConceptsAdapter extends RecyclerView.Adapter<ConceptsAdapter.ConceptsViewHolder> {
    private List<HomeItemsModel> items = new ArrayList<>();
    private Activity context;
    private LayoutInflater inflater;

    public ConceptsAdapter(Activity context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        items.add(new HomeItemsModel("our concepts", "", R.drawable.concept01));
        items.add(new HomeItemsModel("the lemon tree & co.", "katameya", R.drawable.concept02));
        items.add(new HomeItemsModel("the lemon tree & co.", "marassi", R.drawable.concept03));
        items.add(new HomeItemsModel("the beach bar by the", "lemon tree & co.", R.drawable.concept04));
        items.add(new HomeItemsModel("family & friends", "", R.drawable.concept05));
        items.add(new HomeItemsModel("club clandestine", "", R.drawable.concept06));
    }


    @Override
    public ConceptsAdapter.ConceptsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.index_concpets, parent, false);
        return new ConceptsAdapter.ConceptsViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ConceptsViewHolder holder, final int position) {
        HomeItemsModel item = items.get(position);

        if (position == 0) {
            holder.indexItemTitleTextView.setTextColor(ContextCompat.getColor(context, android.R.color.black));
        } else {
            holder.indexItemTitleTextView.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        }

        holder.indexImageView.setImageResource(item.getItemImage());
        holder.indexItemTitleTextView.setText(item.getItemName());
        holder.indexItemTextView.setText(item.getItemDescription());

        if (position != 0) {
            holder.indexParentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ConceptsListActivityNew) context).startTargetActivity(position - 1);
                }
            });
        }
        FontsUtils.setTypeFaceForSubViews(holder.parentView, Constants.LEMON_TREE_BOLD);


    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public class ConceptsViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout indexParentLayout;
        private ImageView indexImageView;
        private RelativeLayout parentView;
        private TextView indexItemTitleTextView, indexItemTextView;

        public ConceptsViewHolder(View itemView) {
            super(itemView);
            indexParentLayout = (LinearLayout) itemView.findViewById(R.id.index_layout);
            indexImageView = (ImageView) itemView.findViewById(R.id.index_imageView);
            indexItemTitleTextView = (TextView) itemView.findViewById(R.id.index_title_textView);
            indexItemTextView = (TextView) itemView.findViewById(R.id.index_subtitles_textView);
            indexItemTextView = (TextView) itemView.findViewById(R.id.index_subtitles_textView);
            parentView = (RelativeLayout) itemView.findViewById(R.id.index_parent_layout);

        }
    }

    public void setData(List<HomeItemsModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
