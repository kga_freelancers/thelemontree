package com.NatSolutions.TheLemonTree.view.view.events;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.EventItem;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.NatSolutions.TheLemonTree.R.drawable.temp;
import static com.NatSolutions.TheLemonTree.R.string.dateFormat;

public class RealEventDetailesActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ImageView eventImageView, shareImageView;
    private TextView eventNameTextView, eventDateTextView, remaningTimeTextView,
            attendenceTextView, eventDescriptionTextView, locationTextView;

    private EventItem eventItem;
    private RelativeLayout mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_event_detailes);
        initializeViews();
        eventItem = (EventItem) getIntent().getSerializableExtra("eventItem");
        fillViewData();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FontsUtils.setTypeFaceForSubViews(mainView, Constants.LEMON_TREE_BOLD);

    }

    private void fillViewData() {
        Picasso.with(this)
                .load(eventItem.EventPhotoPath)
                .into(eventImageView);
        eventNameTextView.setText(eventItem.EventName);
        eventDateTextView.setText(getDayDate(eventItem.EventDate));
        attendenceTextView.setText(eventItem.EventMaxCapacity + " attending");
        eventDescriptionTextView.setText(eventItem.EventDescription);
        locationTextView.setText(eventItem.EventLocation);
        remaningTimeTextView.setText(getElipisedTime());

        shareImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
                i.putExtra(Intent.EXTRA_TEXT, eventItem.EventName);
                startActivity(Intent.createChooser(i, "Share URL"));
            }
        });


    }


    private void initializeViews() {
        mainView = (RelativeLayout) findViewById(R.id.main_view);
        eventImageView = (ImageView) findViewById(R.id.event_imageView);
        eventDateTextView = (TextView) findViewById(R.id.date_textView);
        eventNameTextView = (TextView) findViewById(R.id.event_name_textView);
        remaningTimeTextView = (TextView) findViewById(R.id.remaning_time_textView);
        attendenceTextView = (TextView) findViewById(R.id.attendece_textView);
        eventDescriptionTextView = (TextView) findViewById(R.id.event_description_textView);
        locationTextView = (TextView) findViewById(R.id.location_textView);
        shareImageView = (ImageView) findViewById(R.id.share_imaeView);
    }

    private String getDayDate(String date) {
        date = date.substring(0, 10);
        String[] tempArr = date.split("-");

        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        String dayName = symbols.getWeekdays()[(Integer.parseInt(tempArr[2]) + 1) % 7];
        String monthName = symbols.getMonths()[(Integer.parseInt(tempArr[1]) + 1) % 12];


        final SimpleDateFormat sdf = new SimpleDateFormat("H:mm:ss");
        Date dateObj = null;
        try {
            dateObj = sdf.parse(eventItem.EventTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String am_pm = "";
        if (Integer.parseInt(eventItem.EventTime.substring(0, 2)) > 12)
            am_pm = "PM";
        else
            am_pm = "AM";


        return dayName.substring(0, 3) + "," + tempArr[2] + " " +
                monthName.substring(0, 3).toUpperCase() + " " +
                tempArr[0] + ", " + new SimpleDateFormat("K:mm").format(dateObj) + " " + am_pm;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            LatLng latLng = new LatLng(Double.parseDouble(eventItem.Latitude), Double.parseDouble(eventItem.Longitude));
            googleMap.addMarker(new MarkerOptions().position(latLng));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15), 2000, null);
        } catch (Exception e) {

        }
    }

    private String getElipisedTime() {

        String eventTime = eventItem.EventDate.replace("T", " ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String currentDate = sdf.format(now);
        Date current = null;
        Date event = null;
        try {
            current = sdf.parse(currentDate);
            event = sdf.parse(eventTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getDifference(current, event);
    }


    public String getDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        return elapsedDays + " Days " + elapsedHours + " Hours " + elapsedMinutes + " Mins " + elapsedSeconds + " Secs";
    }
}
