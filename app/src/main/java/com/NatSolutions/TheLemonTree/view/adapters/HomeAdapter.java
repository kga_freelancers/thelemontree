package com.NatSolutions.TheLemonTree.view.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.NatSolutions.TheLemonTree.R;
import com.NatSolutions.TheLemonTree.model.Constants;
import com.NatSolutions.TheLemonTree.model.GuestList;
import com.NatSolutions.TheLemonTree.model.HomeItemsModel;
import com.NatSolutions.TheLemonTree.utils.DataUtil;
import com.NatSolutions.TheLemonTree.utils.FontsUtils;
import com.NatSolutions.TheLemonTree.view.MusicListActivity;
import com.NatSolutions.TheLemonTree.view.UIManager;
import com.NatSolutions.TheLemonTree.view.view.HomeFragment;

import java.util.ArrayList;
import java.util.List;

import static com.NatSolutions.TheLemonTree.model.Constants.NOTIFICATION_RECEIVED;

/**
 * Created by karim on 22/07/17.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {
    private List<HomeItemsModel> items = new ArrayList<>();
    private Activity context;
    private HomeFragment fragment;
    private LayoutInflater inflater;
    private boolean isSideMenuOpened = false;

    public HomeAdapter(Activity context, HomeFragment fragment) {
        this.context = context;
        this.fragment = fragment;
        inflater = LayoutInflater.from(context);
        items.add(new HomeItemsModel("concepts", "life give us lemons and we've given \nyou the lemon tree and co.", R.drawable.test3002));
        items.add(new HomeItemsModel("reservations", "life give us lemons and we've given \nyou the lemon tree and co.", R.drawable.test3003));
        items.add(new HomeItemsModel("sound", "the sound of the lemon tree", R.drawable.test3004));
        items.add(new HomeItemsModel("blog", "life give us lemons and we've given \nyou the lemon tree and co.", R.drawable.test3005));
        items.add(new HomeItemsModel("boutique", "life give us lemons and we've given \nyou the lemon tree and co.", R.drawable.test3006));
    }


    @Override
    public HomeAdapter.HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = inflater.inflate(R.layout.index_home, parent, false);
        return new HomeAdapter.HomeViewHolder(row);
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, final int position) {
        HomeItemsModel item = items.get(position);

        if (position == 0) {
            holder.firstItem.setVisibility(View.VISIBLE);
            if (DataUtil.getData(context, NOTIFICATION_RECEIVED, false)) {
                holder.notificationIconView.setVisibility(View.VISIBLE);
            } else {
                holder.notificationIconView.setVisibility(View.GONE);
            }
        } else {
            holder.firstItem.setVisibility(View.GONE);
        }

        if (position == items.size() - 1) {
            holder.lastItem.setVisibility(View.VISIBLE);
        } else {
            holder.lastItem.setVisibility(View.GONE);
        }

        holder.indexImageView.setImageResource(item.getItemImage());
        holder.indexItemTitleTextView.setText(item.getItemName());
        holder.indexItemTextView.setText(item.getItemDescription());

        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position) {
                    case 0:
                        UIManager.startConceptsActivity((context));
                        break;
                    case 1:
                        UIManager.startReservationActivity(context, null, null);
                        break;
                    case 2:
                        context.startActivity(new Intent(context, MusicListActivity.class));
                        break;
                    case 3:
//                        context.startActivity(new Intent(context, EventListActivity.class));
                       openSoonAlert();
                        break;
                    case 4:
                        openSoonAlert();
                        break;

                }
            }
        });

        holder.aboutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIManager.startAboutUs(context);
            }
        });

        holder.facebookImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri;
                try {
                    PackageManager pm = context.getPackageManager();
                    pm.getPackageInfo("com.facebook.katana", 0);
                    // http://stackoverflow.com/a/24547437/1048340
                    uri = Uri.parse("fb://facewebmodal/f?href=" + "https://www.facebook.com/TheLemonTreeCairo");
                } catch (PackageManager.NameNotFoundException e) {
                    uri = Uri.parse("https://www.facebook.com/TheLemonTreeCairo");
                }

                context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
            }

        });

        holder.instagramImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://instagram.com/_u//thelemontreeandco/");
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    context.startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://instagram.com//thelemontreeandco/")));
                }
            }
        });

        holder.settingImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //UIManager.startUserProfileSettings(context);
                isSideMenuOpened = true;
                notifyDataSetChanged();
            }
        });

        FontsUtils.setTypeFaceForSubViews(holder.indexParentLayout, Constants.LEMON_TREE_BOLD);
        FontsUtils.setTypeFace(holder.indexItemTextView, Constants.LEMON_TREE_REGULAR);

        if (isSideMenuOpened) {
            fragment.openSideMenu();
        }

    }

    public boolean openSideMenu() {
        return isSideMenuOpened;
    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout indexItemContainer, firstItem, indexParentLayout;
        private LinearLayout itemLayout;
        private FrameLayout lastItem;
        private ImageView indexImageView;
        private TextView indexItemTitleTextView, indexItemTextView;
        private LinearLayout aboutLayout;
        private ImageView settingImageView;
        private ImageView facebookImageView;
        private ImageView instagramImageView;
        private View notificationIconView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            firstItem = (RelativeLayout) itemView.findViewById(R.id.relative0);
            indexParentLayout = (RelativeLayout) itemView.findViewById(R.id.index_parent_layout);
            lastItem = (FrameLayout) itemView.findViewById(R.id.last_item);
            itemLayout = (LinearLayout) itemView.findViewById(R.id.index_layout);
            indexImageView = (ImageView) itemView.findViewById(R.id.index_imageView);
            indexItemTitleTextView = (TextView) itemView.findViewById(R.id.index_textView);
            indexItemTextView = (TextView) itemView.findViewById(R.id.index_subtitles_textVie);
            aboutLayout = (LinearLayout) itemView.findViewById(R.id.about_layout);
            settingImageView = (ImageView) itemView.findViewById(R.id.profile_settings);
            facebookImageView = (ImageView) itemView.findViewById(R.id.facebook_imageview);
            instagramImageView = (ImageView) itemView.findViewById(R.id.instagram_imageView);
            notificationIconView = itemView.findViewById(R.id.notification_icon);

        }
    }

    private void openSoonAlert() {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.alarm_txt_dialog);

        final TextView text1 = (TextView) dialog.findViewById(R.id.text1);

        Button dialogButton = (Button) dialog.findViewById(R.id.add_guest_btn);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuestList item = new GuestList();
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void setData(List<HomeItemsModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
